package huornforest.server;

import huornforest.common.HuornForest;
import huornforest.world.WorldGenHuornIsland;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.src.ModLoader;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.world.Teleporter;

public class CommandTeleportDimension implements ICommand
{
    private List aliases;

    public CommandTeleportDimension()
    {
        this.aliases = new ArrayList();
        this.aliases.add("changedimension");
        this.aliases.add("dimension");
        this.aliases.add("dim");
    }

    @Override
    public String getCommandName()
    {
        return "changedimension";
    }

    @Override
    public String getCommandUsage(ICommandSender icommandsender)
    {
        return "/changedimension [dimensionID]";
    }

    @Override
    public List getCommandAliases()
    {
        return this.aliases;
    }

    @Override
    public void processCommand(ICommandSender icommandsender, String[] astring)
    {
        EntityPlayer player = CommandBase.getCommandSenderAsPlayer(icommandsender);
		if ((player.ridingEntity == null) 
		&& (player.riddenByEntity == null) 
		&& ((player instanceof EntityPlayerMP)))
        {
	    	int dimensionID;
	    	
	        if (astring.length > 0 && astring[0].matches("-?[0-9]*"))
	        {
	            dimensionID = Integer.parseInt(astring[0]);
	        } 
	        else
	        {        
	        	dimensionID = 0;
	        }

			EntityPlayerMP thePlayer = (EntityPlayerMP)player;
            thePlayer.timeUntilPortal = 10;
            thePlayer.mcServer.getConfigurationManager()
            		.transferPlayerToDimension(thePlayer, dimensionID, 
            		new Teleporter(thePlayer.mcServer.worldServerForDimension(dimensionID)));
        }
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender icommandsender)
    {
        return true;
    }

    @Override
    public List addTabCompletionOptions(ICommandSender icommandsender, String[] astring)
    {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] astring, int i)
    {
        return false;
    }

    @Override
    public int compareTo(Object o)
    {
        return 0;
    }
}