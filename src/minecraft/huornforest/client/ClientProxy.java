package huornforest.client;

import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderPig;
import net.minecraft.client.renderer.entity.RenderSnowball;
import net.minecraft.item.Item;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import huornforest.common.HuornForest;
import huornforest.common.CommonProxy;
import huornforest.entities.EntityFairyMob;
import huornforest.entities.EntityGrenade;
import huornforest.entities.RenderFairyMob;
import huornforest.entities.RenderFairyModel;
import huornforest.entities.RenderQuartzLight;

public class ClientProxy extends CommonProxy
{
    @Override
    public void registerRenderers()
    {        
    	TickRegistry.registerTickHandler(new TickHandler(), Side.CLIENT);
        HuornForest.quartzLightModelID = RenderingRegistry.getNextAvailableRenderId();
        //RenderingRegistry.registerBlockHandler(HuornForest.quartzLightModelID, new RenderQuartzLight());
        RenderingRegistry.registerEntityRenderingHandler(EntityGrenade.class, new RenderSnowball(HuornForest.itemHuornSplashPotion, 255));
        RenderingRegistry.registerEntityRenderingHandler(EntityFairyMob.class, new RenderFairyMob(new RenderFairyModel(), 0.2F));
    }
}