package huornforest.client;

import huornforest.blocks.brewing.ContainerBrewing;
import huornforest.blocks.brewing.GuiBrewing;
import huornforest.blocks.brewing.TileEntityBrewing;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler{

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tile_entity = world.getBlockTileEntity(x, y, z);
		
		if(tile_entity instanceof TileEntityBrewing){
			return new ContainerBrewing(player.inventory, (TileEntityBrewing) tile_entity);
		}

		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tile_entity = world.getBlockTileEntity(x, y, z);
		
		if(tile_entity instanceof TileEntityBrewing){
			return new GuiBrewing(player.inventory, (TileEntityBrewing) tile_entity);
		}
		
		return null;
	}

}
