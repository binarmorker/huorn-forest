package huornforest.client;

import huornforest.common.ArmorHook;
import huornforest.common.HuornForest;

import java.util.EnumSet;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.TickType;

public class TickHandler implements ITickHandler
{
	private boolean seen;
	private int counter = 0;
	
    @Override
    public void tickStart(EnumSet<TickType> type, Object... tickData) { }

    @Override
    public void tickEnd(EnumSet<TickType> type, Object... tickData)
    {
    	if (type.equals(EnumSet.of(TickType.RENDER))) {
    		onRenderTick();
    	}
        if (type.equals(EnumSet.of(TickType.PLAYER))) {
        	if (!seen) {
        		EntityPlayer player = (EntityPlayer)tickData[0];
				if (Version.needsUpdateNoticeAndMarkAsSeen()) {
					player.addChatMessage(String.format("\u00A7c" + StatCollector.translateToLocal("huornforest.newversion1") + " Huorn Forest " + StatCollector.translateToLocal("huornforest.newversion2") + " v%s " + StatCollector.translateToLocal("huornforest.newversion3") + " Minecraft %s", Version.getRecommendedVersion(), Loader.instance().getMinecraftModContainer().getVersion()));
					seen = true;
				}
        	}
        } 
        if (type.equals(EnumSet.of(TickType.PLAYER))) {
    		if (((EntityPlayer)tickData[0]).worldObj.rand.nextInt(150) == 0) {
    			ArmorHook.onBadWeather(((EntityPlayer)tickData[0]).worldObj, ((EntityPlayer)tickData[0]));
    		}
        }
    }

    @Override
    public EnumSet<TickType> ticks()
    {
        return EnumSet.of(TickType.RENDER, TickType.PLAYER);
    }

    @Override
    public String getLabel()
    {
        return "Huorn Forest";
    }

    private void onRenderTick()
    {
        if (Minecraft.getMinecraft().theWorld != null)
        {
            HuornForest.blockHuornLeaves.setGraphicsLevel(Minecraft.getMinecraft().gameSettings.fancyGraphics);
        }
    }
}