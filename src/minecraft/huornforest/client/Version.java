package huornforest.client;

import huornforest.common.HuornForest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;

import net.minecraft.client.Minecraft;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraftforge.common.Property;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;

public class Version implements Runnable {
	private static Version instance = new Version();

	public enum EnumUpdateState 
	{
		CURRENT, OUTDATED, DEV, CONNECTION_ERROR
	}

	public static final String VERSION = "0.7.2";
	private static final String REMOTE_VERSION_FILE = "http://huornforest.kitaiweb.ca/version.txt";

	public static EnumUpdateState currentVersion = EnumUpdateState.CURRENT;

	private static String recommendedVersion;
	private static String[] cachedChangelog;

	public static String getVersion() 
	{
		return VERSION;
	}

	public static boolean isOutdated() 
	{
		return currentVersion == EnumUpdateState.OUTDATED;
	}

	public static boolean needsUpdateNoticeAndMarkAsSeen() 
	{
		if (!isOutdated())
			return false;
		return true;
	}
	
	public static String getRecommendedVersion() 
	{
		return recommendedVersion;
	}

	public static void versionCheck() 
	{
		HttpURLConnection conn = null;
		try {
			String location = REMOTE_VERSION_FILE;
			while (location != null && !location.isEmpty()) {
				URL url = new URL(location);
				conn = (HttpURLConnection) url.openConnection();
				conn.setRequestProperty(
						"User-Agent",
						"Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)");
				conn.connect();
				location = conn.getHeaderField("Location");
			}

			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = null;
			String mcVersion = Loader.instance().getMinecraftModContainer().getDisplayVersion();

			if (mcVersion == "vnull") throw (new Exception());
			
			while ((line = reader.readLine()) != null) 
			{
				if (line.startsWith(mcVersion)) {
					if (line.contains("HuornForest")) {
						String[] tokens = line.split(":");
						recommendedVersion = tokens[2];
						
						String[] vCharLocal = VERSION.split("\\.");
						int[] vTokensLocal = new int[vCharLocal.length];
						for (int i=0; i<vTokensLocal.length; i++) {
							vTokensLocal[i] = Integer.parseInt(vCharLocal[i]);
						}
						
						String[] vCharNet = recommendedVersion.split("\\.");
						int[] vTokensNet = new int[vCharNet.length];
						for (int i=0; i<vTokensNet.length; i++) {
							vTokensNet[i] = Integer.parseInt(vCharNet[i]);
						}
						
						int v1 = Integer.compare(vTokensLocal[0], vTokensNet[0]);
						int v2 = Integer.compare(vTokensLocal[1], vTokensNet[1]);
						int v3 = Integer.compare(vTokensLocal[2], vTokensNet[2]);
						
						if (v1 == 0) {
							if (v2 == 0) {
								if (v3 == 0) {
									currentVersion = EnumUpdateState.CURRENT;
								} else if (v3 > 0) {
									currentVersion = EnumUpdateState.DEV;
								} else if (v3 < 0) {
									currentVersion = EnumUpdateState.OUTDATED;
								}
							} else if (v2 > 0) {
								currentVersion = EnumUpdateState.DEV;
							} else if (v2 < 0) {
								currentVersion = EnumUpdateState.OUTDATED;
							}
						} else if (v1 > 0) {
							currentVersion = EnumUpdateState.DEV;
						} else if (v1 < 0) {
							currentVersion = EnumUpdateState.OUTDATED;
						}
					}
				}
			}
			
			if (currentVersion == EnumUpdateState.CURRENT)
				HuornForest.log("Using the latest version ["+ getVersion() + "] for Minecraft " + mcVersion, true, Level.FINER);
			else if (currentVersion == EnumUpdateState.OUTDATED)
				HuornForest.log("Using outdated version [" + VERSION + "] for Minecraft " + mcVersion + ". Consider updating.", true, Level.WARNING);
			else if (currentVersion == EnumUpdateState.DEV)
				HuornForest.log("Using development version [" + VERSION + "] for Minecraft " + mcVersion + ".", true, Level.INFO);

	    } catch (Exception e1) {
			HuornForest.log("Unable to read from remote version authority.", true, Level.WARNING);
			HuornForest.log(e1.toString(), true, Level.WARNING);
	        e1.printStackTrace();
			currentVersion = EnumUpdateState.CONNECTION_ERROR;
	        
	    } finally {
	        if(null != conn) { conn.disconnect(); }
	    }
	}

	@Override
	public void run() 
	{
		int count = 0;
		currentVersion = null;

		HuornForest.log("Beginning version check", true, Level.FINER);

		try {
			while ((count < 3)
					&& ((currentVersion == null) || (currentVersion == EnumUpdateState.CONNECTION_ERROR))) {
				versionCheck();
				count++;

				if (currentVersion == EnumUpdateState.CONNECTION_ERROR) {
					HuornForest.log("Version check attempt " + count + " failed, trying again in 10 seconds", true, Level.INFO);
					Thread.sleep(10000);
				}
			}
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (currentVersion == EnumUpdateState.CONNECTION_ERROR) {
			HuornForest.log("Version check failed", true, Level.INFO);
		}

	}

	public static void check() 
	{
		new Thread(instance).start();
	}
}
