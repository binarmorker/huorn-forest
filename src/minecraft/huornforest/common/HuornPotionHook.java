package huornforest.common;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;

public class HuornPotionHook
{
	@ForgeSubscribe
	public void onBurnDamage(LivingAttackEvent event)
	{
		if(event.entity instanceof EntityPlayer)
	    {
	    	EntityPlayer player = (EntityPlayer)event.entity;
	    	if((event.source.equals(DamageSource.inFire)
	        ||  event.source.equals(DamageSource.onFire))
	        &&  player.isPotionActive(HuornForest.napalm))
	        {
	    		event.setCanceled(true);
				player.extinguish();
	        }
	    }
	}

    @ForgeSubscribe
    public void onEntityUpdate(LivingUpdateEvent event)
    {
        if (event.entityLiving.isPotionActive(HuornForest.creeper) && event.entityLiving.isDead)
        {
            double x = event.entityLiving.posX;
            double y = event.entityLiving.posY;
            double z = event.entityLiving.posZ;
            event.entityLiving.worldObj.newExplosion(event.entityLiving, x, y, z, (event.entityLiving.getActivePotionEffect(HuornForest.creeper).getAmplifier() * 2) - 1, false, true);
        }
        
        if (event.entityLiving.isPotionActive(HuornForest.napalm))
        {
            int x = (int) Math.floor(event.entityLiving.posX);
            int y = (int)(event.entityLiving.posY - event.entityLiving.getYOffset());
            int z = (int) Math.floor(event.entityLiving.posZ);
            Random rand = event.entityLiving.worldObj.rand;
            int force = event.entityLiving.getActivePotionEffect(HuornForest.napalm).getAmplifier();
    		int blockX = 0;
    		int blockY = 0;
    		int blockZ = 0;
            for (int i=0; i<6+(force*2); i++) {
                for (int j=0; j<6+(force*2); j++) {
                    for (int k=0; k<2+(force*3); k++) {
                    	if (rand.nextInt(500 - (force * 50)) == 0) {
                    		blockX = x - 1 - force + i;
                    		blockY = y - 1 - force + j;
                    		blockZ = z - 1 - force + k;
                    		if (event.entityLiving.worldObj.isAirBlock(blockX, blockY, blockZ) && 
                    				!(blockX == x && blockY == y && blockZ == z)) {
                    			event.entityLiving.worldObj.setBlock(blockX, blockY, blockZ, Block.fire.blockID);
                    		}
                    	}
                    }
                }
            }
        }

        if (event.entityLiving.isPotionActive(HuornForest.jesus))
        {
            int x = (int) Math.floor(event.entityLiving.posX);
            int y = (int)(event.entityLiving.posY - event.entityLiving.getYOffset());
            int z = (int) Math.floor(event.entityLiving.posZ);

            if ((event.entityLiving.worldObj.getBlockMaterial(x, y - 1, z) == Material.water
                    || event.entityLiving.worldObj.getBlockMaterial(x, y - 1, z) == Material.lava)
                    && event.entityLiving.worldObj.isAirBlock(x, y, z))
            {
                if (event.entityLiving.motionY < 0 && event.entityLiving.boundingBox.minY < y)
                {
                    event.entityLiving.motionY = 0;
                    event.entityLiving.motionX *= 1.075F;
                    event.entityLiving.motionZ *= 1.075F;
                    event.entityLiving.fallDistance = 0;
                    event.entityLiving.onGround = true;

                    if (event.entityLiving.isSneaking())
                    {
                        event.entityLiving.motionY -= 0.15F;
                    }
                }
            }
        }
    }
    
    public static void randomTeleport(EntityLivingBase entity) 
    {
    	randomTeleport(entity, 1);
    }
    
    public static void randomTeleport(EntityLivingBase entity, int force) 
    {
    	for (int n=0; n<10; n++) {
    		HuornForest.debug((n+1) + "th teleportation try.");
	    	int x = (int)Math.round(entity.posX);
	    	int y = (int)Math.round(entity.posY);
	    	int z = (int)Math.round(entity.posZ);
	    	int height = entity.worldObj.getActualHeight();
	    	HuornForest.debug(entity.getEntityName() + " is at coordinates " + x + ", " + y + ", " + z + ".");
	    	Random rand = entity.worldObj.rand;
	    	int x1 = (int)Math.round(entity.posX);
	    	int z1 = (int)Math.round(entity.posZ);
	    	int newX = x1 + (force * 20) - rand.nextInt(force * 40);
	    	int newZ = z1 + (force * 20) - rand.nextInt(force * 40);
	    	HuornForest.debug("Trying with X,Z coordinates " + newX + ", " + newZ + ".");
	    	//for (int newY=height; newY>0; newY--) {
	    		int newY = entity.worldObj.getFirstUncoveredBlock(newX, newZ);
	    		if (!entity.worldObj.isAirBlock(newX, newY, newZ)) {
	    	    	HuornForest.debug("Next available coordinates are " + newX + ", " + (newY + 1) + ", " + newZ + ".");
	    	    	entity.worldObj.playAuxSFX(2002, x, y, z, 0);
	    	    	entity.setPositionAndUpdate(newX, newY + 1, newZ);
	    	    	entity.worldObj.playAuxSFX(2002, newX, newY, newZ, 0);
	    	    	HuornForest.log(entity.getEntityName()+" teleported!");
	    	    	x = (int)Math.round(entity.posX);
	    	    	y = (int)Math.round(entity.posY);
	    	    	z = (int)Math.round(entity.posZ);
	    	    	HuornForest.debug(entity.getEntityName() + " is now at coordinates " + x + ", " + y + ", " + z + ".");
	    	    	if (entity instanceof EntityPlayer) { 
	    	    		EntityPlayer player = (EntityPlayer)(entity);
	    	    		player.addChatMessage("You were teleported!");
	    	    	}
	    			//return;
	    		}
	    	//}
    	}
    	if (entity instanceof EntityPlayer) { 
    		EntityPlayer player = (EntityPlayer)(entity);
    		player.addChatMessage("You can't teleport!");
    	}
    	HuornForest.log("No available teleportation spot was found.");
    }
}