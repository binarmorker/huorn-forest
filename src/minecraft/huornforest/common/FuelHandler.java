package huornforest.common;

import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.IFuelHandler;
public class FuelHandler implements IFuelHandler
{
    @Override
    public int getBurnTime(ItemStack fuel)
    {        
        if (fuel.itemID == HuornForest.itemHuornSap.itemID)
        {
            return 1200; // 2x of coal
        }

        if (fuel.itemID == HuornForest.itemHuornLeaf.itemID)
        {
            return 200; // 1/3x of coal
        }

        return 0;
    }
}