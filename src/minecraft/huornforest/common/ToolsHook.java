package huornforest.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.resources.LanguageManager;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import huornforest.items.ItemHuornAxe;
import huornforest.items.ItemHuornHoe;
import huornforest.items.ItemHuornPickaxe;
import huornforest.items.ItemHuornSpade;
import huornforest.items.ItemHuornSword;

public class ToolsHook
{
	public enum Effect {
	    FORTUNE, TRANSFUSION, SMELT
	}
	
	public static EnumToolMaterial getToolMaterial(Item item) {
		if (item instanceof ItemHuornSword) {
			return ((ItemHuornSword)item).toolMaterial;
		} else if (item instanceof ItemHuornSpade) {
			return ((ItemHuornSpade)item).toolMaterial;
		} else if (item instanceof ItemHuornPickaxe) {
			return ((ItemHuornPickaxe)item).toolMaterial;
		} else if (item instanceof ItemHuornHoe) {
			return ((ItemHuornHoe)item).theToolMaterial;
		} else if (item instanceof ItemHuornAxe) {
			return ((ItemHuornAxe)item).toolMaterial;
		} else {
			return null;
		}
	}
	
	public static Effect getToolEffect(EnumToolMaterial material) {
		if(material == HuornForest.LIVINGWOOD || material == HuornForest.LIVINGMETAL) {
			return Effect.TRANSFUSION;
		} else if(material == HuornForest.PEARL || material == HuornForest.AMETHYST) {
			return Effect.FORTUNE;
		} else if(material == HuornForest.MAGMA || material == HuornForest.OBSIDIAN) {
			return Effect.SMELT;
		} else {
			return null;
		}
	}
	
	@ForgeSubscribe
	public void onEntityDeath(LivingDropsEvent event) {
		if (event.source.getSourceOfDamage() instanceof EntityPlayerMP) {
			EntityLiving mob = (EntityLiving)(event.entityLiving);
			EntityPlayerMP player = (EntityPlayerMP)(event.source.getSourceOfDamage());
			ItemStack currentItem = player.inventory.getCurrentItem();
			
			if (currentItem == null) {
				return;
			}
			
			EnumToolMaterial material = getToolMaterial(currentItem.getItem());

			if (getToolEffect(material) == Effect.SMELT) {
				for (int i=0; i<event.drops.size(); i++) {
	                if (FurnaceRecipes.smelting().getSmeltingResult(event.drops.get(i).getEntityItem()) != null) {
	            		ItemStack smelted = FurnaceRecipes.smelting().getSmeltingResult(event.drops.get(i).getEntityItem()).copy();
	            		EntityItem item = new EntityItem(event.drops.get(i).worldObj, event.drops.get(i).posX, event.drops.get(i).posY, event.drops.get(i).posZ, smelted);
						event.drops.set(i, item);
	                }
				}
				
			} else if (getToolEffect(material) == Effect.FORTUNE) {
				int dropRate = (int)Math.ceil(2 + ((currentItem.getItemDamage() / currentItem.getMaxDamage()) * 10));
				if (mob.worldObj.rand.nextInt(dropRate) == 0) {
					for (int i=0; i<event.drops.size(); i++) {
						ItemStack newItem = new ItemStack(event.drops.get(i).getEntityItem().getItem(), event.drops.get(i).getEntityItem().stackSize * 2);
						EntityItem item = new EntityItem(event.drops.get(i).worldObj, event.drops.get(i).posX, event.drops.get(i).posY, event.drops.get(i).posZ, newItem);
						event.drops.set(i, item);
					}
				}
				
			} else if (getToolEffect(material) == Effect.TRANSFUSION) {
				float exp = mob.getMaxHealth();
				player.heal(exp * 0.1F);
			}
		}
	}
	
	public static boolean onBlockStartBreak(EnumToolMaterial material, ItemStack itemstack, int x, int y, int z, EntityPlayer player) {
		World world = player.worldObj;
		Block block = Block.blocksList[world.getBlockId(x, y, z)];
        int meta = world.getBlockMetadata(x, y, z);
		NBTTagList ench = itemstack.getEnchantmentTagList();
		int level = 0;
		
		if(ench != null) {
            for (int i = 0; i < ench.tagCount(); i++) {
                NBTTagCompound nbt = (NBTTagCompound)ench.tagAt(i);
                int id = nbt.getInteger("id");
                if (id == Enchantment.fortune.effectId) {
                    level = nbt.getInteger("lvl");
                }
            }
    	}
		
		ArrayList<ItemStack> blockDrops = block.getBlockDropped(world, x, y, z, meta, level);

        if(blockDrops == null || blockDrops.size() == 0) {
            return false;
        }

		if (getToolEffect(material) == Effect.SMELT) {
			for (ItemStack item : blockDrops) {
                if (FurnaceRecipes.smelting().getSmeltingResult(item) != null) {
                    ItemStack smelted = FurnaceRecipes.smelting().getSmeltingResult(item).copy();
                    dropItem(world, itemstack, x, y, z, player, smelted);
                } else {
                    dropItem(world, itemstack, x, y, z, player, item);
                }
            }
			
		} else if (getToolEffect(material) == Effect.FORTUNE) {
			int dropRate = (int)Math.ceil(2 + ((itemstack.getItemDamage() / itemstack.getMaxDamage()) * 10));
			if (world.rand.nextInt(dropRate) == 0) {
				for (ItemStack item : blockDrops) {
	                ItemStack dupe = ItemStack.copyItemStack(item);
	                dupe.stackSize *= 2;
	                dropItem(world, itemstack, x, y, z, player, dupe);
				}
			} else {
				return false;
			}
			
		}  else if (getToolEffect(material) == Effect.TRANSFUSION) {
			float hard = block.blockHardness;
			player.heal(hard * 0.025F);
			for (ItemStack item : blockDrops) {
            	dropItem(world, itemstack, x, y, z, player, item);
			}
		} else {
			return false;
		}
		
		return true;
	}
	
	public static float getStrVsBlock(ItemStack itemstack, Block block) {
		float efficiency = 0.0F;
		EnumToolMaterial material = getToolMaterial(itemstack.getItem());
		
		if (material == HuornForest.OBSIDIAN 
		&& itemstack.getItem() instanceof ItemHuornPickaxe
		&& block.blockID == Block.obsidian.blockID) {
			efficiency = itemstack.getStrVsBlock(block) * 50.0F;
		}
		
		return efficiency;
	}
	
	private static void dropItem(World world, ItemStack itemstack, int x, int y, int z, EntityPlayer player, ItemStack droppedItem) {
        world.playSoundEffect(x + 0.5F, y + 0.5F, z + 0.5F,
            Block.blocksList[world.getBlockId(x, y, z)].stepSound.getBreakSound(),
            (Block.blocksList[world.getBlockId(x, y, z)].stepSound.getVolume() + 1.0F) / 2.0F,
            Block.blocksList[world.getBlockId(x, y, z)].stepSound.getPitch() * 0.8F);
        world.setBlock(x, y, z, 0);

        if(!world.isRemote) {
            float var6 = 0.7F;
            double var7 = world.rand.nextFloat() * var6 + 1.0F - var6 * 0.5D;
            double var9 = world.rand.nextFloat() * var6 + 1.0F - var6 * 0.5D;
            double var11 = world.rand.nextFloat() * var6 + 1.0F - var6 * 0.5D;
            EntityItem entityitem = new EntityItem(world, x + 0.5D, y + 0.5D, z + 0.D, droppedItem);
            world.spawnEntityInWorld(entityitem);
        }

        itemstack.damageItem(1, player);
	}
}