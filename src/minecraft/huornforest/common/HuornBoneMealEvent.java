package huornforest.common;

import huornforest.blocks.BlockHuornSapling;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.BonemealEvent;
import net.minecraft.src.*;

public class HuornBoneMealEvent
{
    @ForgeSubscribe
    public void onUseBonemeal(BonemealEvent event)
    {
        if (event.ID == HuornForest.blockHuornSapling.blockID)
        {
            if (!event.world.isRemote)
            {
                ((BlockHuornSapling)HuornForest.blockHuornSapling).growTree(event.world, event.X, event.Y, event.Z, event.world.rand);
            }
        }
    }
}