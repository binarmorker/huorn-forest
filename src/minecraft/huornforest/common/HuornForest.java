package huornforest.common;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import huornforest.blocks.*;
import huornforest.blocks.brewing.*;
import huornforest.blocks.treetap.*;
import huornforest.entities.*;
import huornforest.items.*;
import huornforest.world.*;
import huornforest.server.*;
import huornforest.common.*;
import huornforest.client.*;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFluid;
import net.minecraft.block.BlockHalfSlab;
import net.minecraft.block.BlockLeavesBase;
import net.minecraft.block.BlockStationary;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSlab;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.potion.Potion;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.ModLoader;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StringTranslate;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.WorldProviderSurface;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.gen.MapGenBase;
import net.minecraftforge.client.EnumHelperClient;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.EnumHelper;
import net.minecraftforge.common.ISpecialArmor.ArmorProperties;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.IGuiHandler;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameData;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid="HuornForest", name="Huorn Forest", version=Version.VERSION)
@NetworkMod(clientSideRequired=true, serverSideRequired=false)
public class HuornForest
{
    @Instance("HuornForest")
    public static HuornForest instance;
    @SidedProxy(clientSide = "huornforest.client.ClientProxy", serverSide = "huornforest.common.CommonProxy")
    public static CommonProxy proxy;
    private static Logger LOGGER = Logger.getLogger("HuornForest");
    private GuiHandler guihandler = new GuiHandler();
	private static int blockHuornLogID;
	private static int blockHuornWoodID;
	private static int blockHuornDoubleSlabID;
	private static int blockHuornSingleSlabID;
	private static int blockHuornStairsID;
	private static int blockHuornSaplingID;
	private static int blockHumusID;
	private static int blockHuornLeavesID;
	private static int blockBlueQuartzID;
	private static int blockClearWaterID;
	/*private static int blockCondensatorOffID;
	private static int blockCondensatorOnID;*/
	private static int blockObsidianOreID;
	private static int blockAmethystOreID;
	private static int blockAmethystID;
	//private static int blockTreetapID;
	private static int blockCopperOreID;
	private static int blockCopperID;
	private static int blockQuartzLightID;
	private static int itemHuornSapID;
	private static int itemHuornAshesID;
	private static int itemHuornLeafID;
	private static int itemClearWaterBucketID;
	private static int itemHuornPotionID;
	private static int itemHuornSplashPotionID;
	private static int itemFairyBottleID;
	private static int itemObsidianID;
	private static int itemPearlID;
	private static int itemAmethystID;
	//private static int itemTreetapID;
	private static int itemGolemIngotID;
	private static int itemMagmaIngotID;
	private static int itemCopperIngotID;
	private static int itemSteelIngotID;
	/**
	 * TOOLS IDS
	 */
	private static int itemCopperSwordID;
	private static int itemCopperAxeID;
	private static int itemCopperPickaxeID;
	private static int itemCopperSpadeID;
	private static int itemCopperHoeID;
	
	private static int itemHuornSwordID;
	private static int itemHuornAxeID;
	private static int itemHuornPickaxeID;
	private static int itemHuornSpadeID;
	private static int itemHuornHoeID;
	
	private static int itemPearlSwordID;
	private static int itemPearlAxeID;
	private static int itemPearlPickaxeID;
	private static int itemPearlSpadeID;
	private static int itemPearlHoeID;
	
	private static int itemGolemSwordID;
	private static int itemGolemAxeID;
	private static int itemGolemPickaxeID;
	private static int itemGolemSpadeID;
	private static int itemGolemHoeID;
	
	private static int itemSteelSwordID;
	private static int itemSteelAxeID;
	private static int itemSteelPickaxeID;
	private static int itemSteelSpadeID;
	private static int itemSteelHoeID;
	
	private static int itemMagmaSwordID;
	private static int itemMagmaAxeID;
	private static int itemMagmaPickaxeID;
	private static int itemMagmaSpadeID;
	private static int itemMagmaHoeID;
	
	private static int itemEmeraldSwordID;
	private static int itemEmeraldAxeID;
	private static int itemEmeraldPickaxeID;
	private static int itemEmeraldSpadeID;
	private static int itemEmeraldHoeID;
	
	private static int itemAmethystSwordID;
	private static int itemAmethystAxeID;
	private static int itemAmethystPickaxeID;
	private static int itemAmethystSpadeID;
	private static int itemAmethystHoeID;
	
	private static int itemObsidianSwordID;
	private static int itemObsidianAxeID;
	private static int itemObsidianPickaxeID;
	private static int itemObsidianSpadeID;
	private static int itemObsidianHoeID;
	/**
	 * ARMOR IDS
	 */
	private static int itemCopperHelmetID;
	private static int itemCopperPlateID;
	private static int itemCopperLegsID;
	private static int itemCopperBootsID;

	private static int itemEmeraldHelmetID;
	private static int itemEmeraldPlateID;
	private static int itemEmeraldLegsID;
	private static int itemEmeraldBootsID;

	private static int itemObsidianHelmetID;
	private static int itemObsidianPlateID;
	private static int itemObsidianLegsID;
	private static int itemObsidianBootsID;
	
	public static int quartzLightModelID;
	
    public static Block blockHuornLog;
    public static Block blockHuornWood;
    public static Block blockHuornDoubleSlab;
    public static Block blockHuornSingleSlab;
    public static Block blockHuornStairs;
    public static Block blockHuornSapling;
    public static Block blockHumus;
    public static BlockHuornLeaves blockHuornLeaves;
    public static Fluid fluidClearWater;
    public static Block blockBlueQuartz;
    public static Block blockClearWater;
    /*public static Block blockCondensatorOff;
    public static Block blockCondensatorOn;*/
    public static Block blockObsidianOre;
    public static Block blockAmethystOre;
    public static Block blockAmethyst;
    //public static Block blockTreetap;
    public static Block blockCopperOre;
    public static Block blockCopper;
    public static Block blockQuartzLight;
    public static Item itemHuornSap;
    public static Item itemHuornAshes;
    public static Item itemHuornLeaf;
    public static Item itemClearWaterBucket;
    public static Item itemHuornPotion;
    public static Item itemHuornSplashPotion;
    public static Item itemFairyBottle;
	public static Item itemObsidian;
	public static Item itemPearl;
	public static Item itemAmethyst;
	//public static Item itemTreetap;
	public static Item itemGolemIngot;
	public static Item itemMagmaIngot;
	public static Item itemSteelIngot;
	public static Item itemCopperIngot;
	/**
	 * TOOLS OBJECTS
	 */
    public static Item itemCopperSword;
    public static Item itemCopperAxe;
    public static Item itemCopperPickaxe;
    public static Item itemCopperSpade;
    public static Item itemCopperHoe;
    
    public static Item itemHuornSword;
    public static Item itemHuornAxe;
    public static Item itemHuornPickaxe;
    public static Item itemHuornSpade;
    public static Item itemHuornHoe;
    
	public static Item itemPearlSword;
	public static Item itemPearlAxe;
	public static Item itemPearlPickaxe;
	public static Item itemPearlSpade;
	public static Item itemPearlHoe;
	
    public static Item itemGolemSword;
    public static Item itemGolemAxe;
    public static Item itemGolemPickaxe;
    public static Item itemGolemSpade;
    public static Item itemGolemHoe;
    
    public static Item itemSteelSword;
    public static Item itemSteelAxe;
    public static Item itemSteelPickaxe;
    public static Item itemSteelSpade;
    public static Item itemSteelHoe;
    
    public static Item itemMagmaSword;
    public static Item itemMagmaAxe;
    public static Item itemMagmaPickaxe;
    public static Item itemMagmaSpade;
    public static Item itemMagmaHoe;
    
    public static Item itemEmeraldSword;
    public static Item itemEmeraldAxe;
    public static Item itemEmeraldPickaxe;
    public static Item itemEmeraldSpade;
    public static Item itemEmeraldHoe;
    
	public static Item itemAmethystSword;
	public static Item itemAmethystAxe;
	public static Item itemAmethystPickaxe;
	public static Item itemAmethystSpade;
	public static Item itemAmethystHoe;
	
    public static Item itemObsidianSword;
    public static Item itemObsidianAxe;
    public static Item itemObsidianPickaxe;
	public static Item itemObsidianSpade;
	public static Item itemObsidianHoe;
	
	/**
	 * ARMOR OBJECTS
	 */
    public static Item itemCopperHelmet;
    public static Item itemCopperPlate;
    public static Item itemCopperLegs;
    public static Item itemCopperBoots;

    public static Item itemEmeraldHelmet;
    public static Item itemEmeraldPlate;
    public static Item itemEmeraldLegs;
    public static Item itemEmeraldBoots;

    public static Item itemObsidianHelmet;
    public static Item itemObsidianPlate;
    public static Item itemObsidianLegs;
    public static Item itemObsidianBoots;
	
    public static Potion creeper;
    public static Potion napalm;
    public static Potion jesus;
    public static Potion aura;
    
    public static boolean canUseCommands;
    public static boolean checkUpdate;
    public static boolean grenadeMode;
    public static boolean verbose;
    
    public static EnumToolMaterial COPPER;
    public static EnumToolMaterial LIVINGWOOD;
    public static EnumToolMaterial PEARL;
    public static EnumToolMaterial LIVINGMETAL;
    public static EnumToolMaterial STEEL;
    public static EnumToolMaterial MAGMA;
    public static EnumToolMaterial RUBY;
    public static EnumToolMaterial AMETHYST;
    public static EnumToolMaterial OBSIDIAN;
    public static EnumArmorMaterial aCOPPER;
    public static EnumArmorMaterial aRUBY;
    public static EnumArmorMaterial aOBSIDIAN;
    
    public static CreativeTabs HuornTab = new CreativeTabs(CreativeTabs.getNextID(),"HuornTab") {
        public ItemStack getIconItemStack() { return new ItemStack(blockHuornLog); }
    };

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	Configuration config = new Configuration(event.getSuggestedConfigurationFile());
    	config.load();
    	blockHuornLogID = config.getBlock("Huorn Log", 500).getInt();
    	blockHuornWoodID = config.getBlock("Huorn Wood", 501).getInt();
    	blockHuornDoubleSlabID = config.getBlock("Huorn Double Slab", 502).getInt();
    	blockHuornSingleSlabID = config.getBlock("Huorn Single Slab", 503).getInt();
    	blockHuornStairsID = config.getBlock("Huorn Stairs", 504).getInt();
    	blockHuornSaplingID = config.getBlock("Huorn Sapling", 505).getInt();
    	blockHumusID = config.getBlock("Humus Block", 506).getInt();
    	blockHuornLeavesID = config.getBlock("Huorn Leaves", 507).getInt();
    	blockBlueQuartzID = config.getBlock("Blue Quartz", 508).getInt();
    	blockClearWaterID = config.getBlock("Clear Water", 509).getInt();
    	/*blockCondensatorOffID = config.getBlock("Condensator Off", 510).getInt();
    	blockCondensatorOnID = config.getBlock("Condensator On", 511).getInt();*/
    	blockObsidianOreID = config.getBlock("Obsidian Ore", 513).getInt();
    	blockAmethystOreID = config.getBlock("Amethyst Ore", 514).getInt();
    	blockAmethystID = config.getBlock("Amethyst", 515).getInt();
    	//blockTreetapID = config.getBlock("Treetap", 516).getInt();
    	blockCopperOreID = config.getBlock("Copper Ore", 517).getInt();
    	blockCopperID = config.getBlock("Copper", 518).getInt();
    	blockQuartzLightID = config.getBlock("Quartz Light", 519).getInt();
    	itemHuornSapID = config.getItem("Huorn Sap", 4000).getInt();
    	itemHuornAshesID = config.getItem("Huorn Ashes", 4001).getInt();
    	itemHuornLeafID = config.getItem("Huorn Leaf", 4002).getInt();
    	itemClearWaterBucketID = config.getItem("Clear Water Bucket", 4003).getInt();
    	itemHuornPotionID = config.getItem("Huorn Potion", 4004).getInt();
    	itemHuornSplashPotionID = config.getItem("Huorn Splash Potion", 4005).getInt();
    	itemFairyBottleID = config.getItem("Fairy Bottle", 4006).getInt();
    	itemObsidianID = config.getItem("Obsidian", 4007).getInt();
    	itemPearlID = config.getItem("Pearl", 4008).getInt();
    	itemAmethystID = config.getItem("Amethyst", 4009).getInt();
    	//itemTreetapID = config.getItem("Treetap", 4010).getInt();
    	itemGolemIngotID = config.getItem("Golem Iron Ingot", 4011).getInt();
    	itemMagmaIngotID = config.getItem("Magma Nether Brick", 4012).getInt();
    	itemSteelIngotID = config.getItem("Steel Ingot", 4013).getInt();
    	itemCopperIngotID = config.getItem("Copper Ingot", 4014).getInt();
    	/**
    	 * TOOLS CONFIG
    	 */
    	itemCopperSwordID = config.getItem("Copper Sword", 4100).getInt();
    	itemCopperAxeID = config.getItem("Copper Axe", 4101).getInt();
    	itemCopperPickaxeID = config.getItem("Copper Pickaxe", 4102).getInt();
    	itemCopperSpadeID = config.getItem("Copper Spade", 4103).getInt();
    	itemCopperHoeID = config.getItem("Copper Hoe", 4104).getInt();
    	
    	itemHuornSwordID = config.getItem("Huorn Wood Sword", 4105).getInt();
    	itemHuornAxeID = config.getItem("Huorn Wood Axe", 4106).getInt();
    	itemHuornPickaxeID = config.getItem("Huorn Wood Pickaxe", 4107).getInt();
    	itemHuornSpadeID = config.getItem("Huorn Wood Spade", 4108).getInt();
    	itemHuornHoeID = config.getItem("Huorn Wood Hoe", 4109).getInt();
    	
    	itemPearlSwordID = config.getItem("Pearl Sword", 4110).getInt();
    	itemPearlAxeID = config.getItem("Pearl Axe", 4111).getInt();
    	itemPearlPickaxeID = config.getItem("Pearl Pickaxe", 4112).getInt();
    	itemPearlSpadeID = config.getItem("Pearl Spade", 4113).getInt();
    	itemPearlHoeID = config.getItem("Pearl Hoe", 4114).getInt();
    	
    	itemGolemSwordID = config.getItem("Golem Metal Sword", 4115).getInt();
    	itemGolemAxeID = config.getItem("Golem Metal Axe", 4116).getInt();
    	itemGolemPickaxeID = config.getItem("Golem Metal Pickaxe", 4117).getInt();
    	itemGolemSpadeID = config.getItem("Golem Metal Spade", 4118).getInt();
    	itemGolemHoeID = config.getItem("Golem Metal Hoe", 4119).getInt();
    	
    	itemSteelSwordID = config.getItem("Steel Sword", 4120).getInt();
    	itemSteelAxeID = config.getItem("Steel Axe", 4121).getInt();
    	itemSteelPickaxeID = config.getItem("Steel Pickaxe", 4122).getInt();
    	itemSteelSpadeID = config.getItem("Steel Spade", 4123).getInt();
    	itemSteelHoeID = config.getItem("Steel Hoe", 4124).getInt();
    	
    	itemMagmaSwordID = config.getItem("Magma Sword", 4125).getInt();
    	itemMagmaAxeID = config.getItem("Magma Axe", 4126).getInt();
    	itemMagmaPickaxeID = config.getItem("Magma Pickaxe", 4127).getInt();
    	itemMagmaSpadeID = config.getItem("Magma Spade", 4128).getInt();
    	itemMagmaHoeID = config.getItem("Magma Hoe", 4129).getInt();
    	
    	itemEmeraldSwordID = config.getItem("Emerald Sword", 4130).getInt();
    	itemEmeraldAxeID = config.getItem("Emerald Axe", 4131).getInt();
    	itemEmeraldPickaxeID = config.getItem("Emerald Pickaxe", 4132).getInt();
    	itemEmeraldSpadeID = config.getItem("Emerald Spade", 4133).getInt();
    	itemEmeraldHoeID = config.getItem("Emerald Hoe", 4134).getInt();
    	
    	itemAmethystSwordID = config.getItem("Amethyst Sword", 4135).getInt();
    	itemAmethystAxeID = config.getItem("Amethyst Axe", 4136).getInt();
    	itemAmethystPickaxeID = config.getItem("Amethyst Pickaxe", 4137).getInt();
    	itemAmethystSpadeID = config.getItem("Amethyst Spade", 4138).getInt();
    	itemAmethystHoeID = config.getItem("Amethyst Hoe", 4139).getInt();
    	
    	itemObsidianSwordID = config.getItem("Obsidian Sword", 4140).getInt();
    	itemObsidianAxeID = config.getItem("Obsidian Axe", 4141).getInt();
    	itemObsidianPickaxeID = config.getItem("Obsidian Pickaxe", 4142).getInt();
    	itemObsidianSpadeID = config.getItem("Obsidian Spade", 4143).getInt();
    	itemObsidianHoeID = config.getItem("Obsidian Hoe", 4144).getInt();
    	
    	/**
    	 * ARMOR CONFIG
    	 */
    	itemCopperHelmetID = config.getItem("Copper Helmet", 4145).getInt();
    	itemCopperPlateID = config.getItem("Copper Chestplate", 4146).getInt();
    	itemCopperLegsID = config.getItem("Copper Leggings", 4147).getInt();
    	itemCopperBootsID = config.getItem("Copper Boots", 4148).getInt();

    	itemEmeraldHelmetID = config.getItem("Emerald Helmet", 4149).getInt();
    	itemEmeraldPlateID = config.getItem("Emerald Chestplate", 4150).getInt();
    	itemEmeraldLegsID = config.getItem("Emerald Leggings", 4151).getInt();
    	itemEmeraldBootsID = config.getItem("Emerald Boots", 4152).getInt();

    	itemObsidianHelmetID = config.getItem("Obsidian Helmet", 4153).getInt();
    	itemObsidianPlateID = config.getItem("Obsidian Chestplate", 4154).getInt();
    	itemObsidianLegsID = config.getItem("Obsidian Leggings", 4155).getInt();
    	itemObsidianBootsID = config.getItem("Obsidian Boots", 4156).getInt();
    	
    	canUseCommands = config.get(Configuration.CATEGORY_GENERAL, "Enable commands", false).getBoolean(false);
    	checkUpdate = config.get(Configuration.CATEGORY_GENERAL, "Check for updates", true).getBoolean(true);
    	grenadeMode = config.get(Configuration.CATEGORY_GENERAL, "Splash Huorn Potions explode on impact", true).getBoolean(true);
    	verbose = config.get(Configuration.CATEGORY_GENERAL, "Verbose / Debug messages", false).getBoolean(false);
    	config.save();
    	log("Configuration loaded!");
    	if (verbose) {
    		debug("Verbose is activated, Prepare to be spammed by debug messages!", Level.WARNING);
    	}
    	if (checkUpdate) Version.check();
    }

    @EventHandler
    public void load(FMLInitializationEvent event) {
    	proxy.registrerOverrides();
        creeper = (new PotionHuorn(32, true, 0)).setIconIndex(7, 1, false).setPotionName("potion.creeper");
        napalm = (new PotionHuorn(33, true, 0)).setIconIndex(0, 0).setPotionName("potion.napalm");
        jesus = (new PotionHuorn(34, false, 0)).setIconIndex(1, 0).setPotionName("potion.jesus");
        aura = (new PotionHuorn(35, false, 0)).setIconIndex(2, 0).setPotionName("potion.aura");
        
        //////////////////////// TOOL SPECS \\\\\\\\\\\\\\\\\\\\\\\\\
        //										    NAME		   TYP DUR	  EFF	DAM	 ENCH     EFFECT
        // GOLD = 		EnumHelper.addToolMaterial("Gold", 			0, 32,   12.0F, 0.0F, 22);
        // WOOD = 		EnumHelper.addToolMaterial("Wood", 			0, 59,   2.0F,  0.0F, 15);
        COPPER = 		EnumHelper.addToolMaterial("Copper", 		0, 62,   5.0F,  0.0F, 0);
        LIVINGWOOD = 	EnumHelper.addToolMaterial("Huorn Wood", 	1, 59,   2.0F,  0.5F, 8);  // Transfusion
        // STONE = 		EnumHelper.addToolMaterial("Stone", 		1, 131,  4.0F,  1.0F, 5);
        PEARL = 		EnumHelper.addToolMaterial("Pearl", 		1, 186,  5.0F,  1.5F, 12); // Fortune
        // IRON = 		EnumHelper.addToolMaterial("Iron", 			2, 250,  6.0F,  2.0F, 14);
        LIVINGMETAL = 	EnumHelper.addToolMaterial("Golem Metal", 	2, 341,  6.5F,  1.5F, 16); // Transfusion
        STEEL = 		EnumHelper.addToolMaterial("Steel", 		2, 514,  7.0F,  2.0F, 0);
        MAGMA = 		EnumHelper.addToolMaterial("Magma", 		2, 300,  5.5F,  4.0F, 10); // Smelt
        RUBY = 			EnumHelper.addToolMaterial("Emerald", 		3, 1042, 6.0F,  2.5F, 0);
        AMETHYST = 		EnumHelper.addToolMaterial("Amethyst", 		3, 1250, 7.0F,  3.5F, 14); // Fortune
        // EMERALD = 	EnumHelper.addToolMaterial("Diamond", 		3, 1561, 8.0F,  3.0F, 10);
        OBSIDIAN = 		EnumHelper.addToolMaterial("Obsidian", 		3, 3466, 12.0F, 4.0F, 3);  // Smelt
        
        /////////////////////// ARMOR SPECS \\\\\\\\\\\\\\\\\\\\\\\\\
        //                                           NAME      DUR            DAM REDUCT  ENCH     EFFECT
        // aCLOTH = 	EnumHelper.addArmorMaterial("Leather", 	5,  new int[]{1, 3, 2, 1}, 15);
        // aGOLD = 		EnumHelper.addArmorMaterial("Gold", 	7,  new int[]{2, 5, 3, 1}, 25);
        aCOPPER = 		EnumHelper.addArmorMaterial("Copper", 	9,  new int[]{2, 5, 4, 2}, 0);  // Lightning rod
        // aIRON = 		EnumHelper.addArmorMaterial("Iron", 	15, new int[]{2, 6, 5, 2}, 9);
        // aCHAIN = 	EnumHelper.addArmorMaterial("Chain", 	15, new int[]{2, 5, 4, 1}, 12);
        aRUBY = 		EnumHelper.addArmorMaterial("Emerald", 	21, new int[]{3, 7, 5, 2}, 0);  // Magnetism
        // aDIAMOND = 	EnumHelper.addArmorMaterial("Diamond", 	33, new int[]{3, 8, 6, 3}, 10);
        aOBSIDIAN = 	EnumHelper.addArmorMaterial("Obsidian", 42, new int[]{4, 10, 7, 3}, 5); // Fire-proof
        
		blockHuornLog = new BlockHuornLog(blockHuornLogID)
				        .setUnlocalizedName("huornLog")
				        .setHardness(2.0F)
				        .setResistance(5.0F)
				        .setLightValue(0.375F)
				        .setStepSound(Block.soundWoodFootstep)
				        .setCreativeTab(HuornTab);
        blockHuornWood = new BlockHuornWood(blockHuornWoodID)
				        .setUnlocalizedName("huornWood")
				        .setHardness(2.0F)
				        .setResistance(5.0F)
				        .setLightValue(0.625F)
				        .setStepSound(Block.soundWoodFootstep)
				        .setCreativeTab(HuornTab);
        blockHuornDoubleSlab = new BlockHuornHalfSlab(blockHuornDoubleSlabID, true)
				        .setUnlocalizedName("huornDoubleSlab")
				        .setHardness(2.0F)
				        .setResistance(5.0F)
				        .setLightValue(0.625F)
				        .setStepSound(Block.soundWoodFootstep)
				        .setCreativeTab(null);
        blockHuornSingleSlab = new BlockHuornHalfSlab(blockHuornSingleSlabID, false)
				        .setUnlocalizedName("huornSingleSlab")
				        .setHardness(2.0F)
				        .setResistance(5.0F)
				        .setLightValue(0.625F)
				        .setStepSound(Block.soundWoodFootstep)
				        .setCreativeTab(HuornTab);
        blockHuornStairs = new BlockHuornStairs(blockHuornStairsID, blockHuornWood, 0)
				        .setUnlocalizedName("huornStairs")
				        .setHardness(2.0F)
				        .setResistance(5.0F)
				        .setLightValue(0.625F)
				        .setStepSound(Block.soundWoodFootstep)
				        .setCreativeTab(HuornTab);
        blockHuornSapling = new BlockHuornSapling(blockHuornSaplingID)
        			    .setUnlocalizedName("huornAshes")
        			    .setCreativeTab(null);
        blockHumus = new BlockHumus(blockHumusID)
			            .setUnlocalizedName("humus")
			            .setHardness(0.3F)
                        .setResistance(0.5F)
			            .setStepSound(Block.soundGrassFootstep)
			            .setCreativeTab(HuornTab);
        blockHuornLeaves = (BlockHuornLeaves)(new BlockHuornLeaves(blockHuornLeavesID))
                        .setUnlocalizedName("huornLeaves")
                        .setHardness(0.5F)
                        .setResistance(2.0F)
                       	.setLightOpacity(1)
                       	.setStepSound(Block.soundGrassFootstep)
                       	.setCreativeTab(HuornTab);
        blockBlueQuartz = new BlockBlueQuartz(blockBlueQuartzID)
			            .setUnlocalizedName("quartzChiseledBlue")
			            .setHardness(5.0F)
			            .setResistance(10.0F)
			            .setCreativeTab(HuornTab);
        fluidClearWater = (Fluid)(new FluidClearWater("clearWater")
        				.setBlockID(blockClearWaterID));
        				FluidRegistry.registerFluid(fluidClearWater);
        blockClearWater = (new BlockClearWater(blockClearWaterID, fluidClearWater, Material.water))
                        .setHardness(100.0F)
                        .setLightOpacity(2)
                        .setUnlocalizedName("clearWater")
						.setCreativeTab(null);
        /*blockCondensatorOff = new BlockBrewing(blockCondensatorOffID, false)
        			    .setHardness(3.5F)
        			    .setResistance(10.0F)
        			    .setUnlocalizedName("condensator")
        			    .setCreativeTab(HuornTab);
        blockCondensatorOn = new BlockBrewing(blockCondensatorOnID, true)
        			    .setHardness(3.5F)
        			    .setResistance(10.0F)
        			    .setUnlocalizedName("condensatorActive")
        			    .setCreativeTab(null);*/
        blockObsidianOre = new BlockHuornGeneric(blockObsidianOreID)
        				.setUnlocalizedName("obsidianOre")
	    				.setHardness(3.5F)
	    				.setResistance(10.0F)
        				.setCreativeTab(HuornTab);
        blockAmethystOre = new BlockHuornGeneric(blockAmethystOreID)
						.setUnlocalizedName("amethystOre")
						.setHardness(3.5F)
						.setResistance(10.0F)
						.setCreativeTab(HuornTab);
        blockAmethyst = new BlockHuornGeneric(blockAmethystID)
						.setUnlocalizedName("amethyst")
						.setHardness(5.0F)
						.setResistance(10.0F)
						.setStepSound(Block.soundMetalFootstep)
						.setCreativeTab(HuornTab);
        /*blockTreetap = new BlockHuornGeneric(blockTreetapID)
						.setUnlocalizedName("treetap");*/
        blockCopperOre = new BlockHuornGeneric(blockCopperOreID)
						.setUnlocalizedName("copperOre")
						.setHardness(1.5F)
						.setResistance(10.0F)
						.setCreativeTab(HuornTab);
        blockCopper = new BlockHuornGeneric(blockCopperID)
						.setUnlocalizedName("copper")
						.setHardness(3.0F)
						.setResistance(10.0F)
						.setStepSound(Block.soundMetalFootstep)
						.setCreativeTab(HuornTab);
        /*blockQuartzLight = new BlockQuartzLight(blockQuartzLightID)
        				.setStepSound(Block.soundGlassFootstep)
        				.setLightValue(1.0F)
        				.setUnlocalizedName("quartzLight")
        				.setHardness(4.0F)
        				.setResistance(8.0F)
        				.setCreativeTab(HuornTab);*/
        itemHuornSap = new ItemCommon(itemHuornSapID)
						.setMaxStackSize(16)
        			    .setUnlocalizedName("huornSap")
        			    .setCreativeTab(HuornTab);
        itemHuornAshes = new ItemPlacable(itemHuornAshesID, blockHuornSapling)
        			    .setUnlocalizedName("huornAshes")
        			    .setCreativeTab(HuornTab);
        itemHuornLeaf = new ItemCommon(itemHuornLeafID)
        			    .setUnlocalizedName("huornLeaf")
        			    .setCreativeTab(HuornTab);
        itemClearWaterBucket = new ItemPlacable(itemClearWaterBucketID, blockClearWater, new ItemStack(Item.bucketEmpty))
        				.setMaxStackSize(1)
        			    .setUnlocalizedName("clearWaterBucket")
        			    .setCreativeTab(HuornTab);
        itemHuornPotion = new ItemHuornPotion(itemHuornPotionID)
						.setUnlocalizedName("potionHuorn")
						.setCreativeTab(HuornTab);
		itemHuornSplashPotion = new ItemSplashHuornPotion(itemHuornSplashPotionID)
						.setUnlocalizedName("splashHuorn")
						.setCreativeTab(HuornTab);
		itemFairyBottle = new ItemFairyBottle(itemFairyBottleID)
			    		.setUnlocalizedName("fairyBottle")
			    		.setCreativeTab(HuornTab);
        itemObsidian = new ItemObsidian(itemObsidianID)
						.setUnlocalizedName("obsidian")
						.setCreativeTab(HuornTab);
        itemPearl = new ItemCommon(itemPearlID)
						.setUnlocalizedName("pearl")
						.setCreativeTab(HuornTab);
        itemAmethyst = new ItemAmethyst(itemAmethystID)
						.setUnlocalizedName("amethyst")
						.setCreativeTab(HuornTab);
        /*itemTreetap = new ItemTreetap(itemTreetapID)
						.setUnlocalizedName("treetap")
						.setCreativeTab(HuornTab);*/
        itemGolemIngot = new ItemCommon(itemGolemIngotID)
						.setUnlocalizedName("golemIronIngot")
						.setCreativeTab(HuornTab);
        itemMagmaIngot = new ItemCommon(itemMagmaIngotID)
						.setUnlocalizedName("magmaBrick")
						.setCreativeTab(HuornTab);
        itemSteelIngot = new ItemCommon(itemSteelIngotID)
						.setUnlocalizedName("steelIngot")
						.setCreativeTab(HuornTab);
        itemCopperIngot = new ItemCommon(itemCopperIngotID)
						.setUnlocalizedName("copperIngot")
						.setCreativeTab(HuornTab);
        /**
         * TOOLS DEFINITIONS
         */
        itemCopperSword = new ItemHuornSword(itemCopperSwordID, COPPER).setUnlocalizedName("swordCopper").setCreativeTab(HuornTab);
        itemCopperAxe = new ItemHuornAxe(itemCopperAxeID, COPPER).setUnlocalizedName("axeCopper").setCreativeTab(HuornTab);
        itemCopperPickaxe = new ItemHuornPickaxe(itemCopperPickaxeID, COPPER).setUnlocalizedName("pickaxeCopper").setCreativeTab(HuornTab);
        itemCopperSpade = new ItemHuornSpade(itemCopperSpadeID, COPPER).setUnlocalizedName("spadeCopper").setCreativeTab(HuornTab);
        itemCopperHoe = new ItemHuornHoe(itemCopperHoeID, COPPER).setUnlocalizedName("hoeCopper").setCreativeTab(HuornTab);
        
        itemHuornSword = new ItemHuornSword(itemHuornSwordID, LIVINGWOOD).setUnlocalizedName("swordHuorn").setCreativeTab(HuornTab);
        itemHuornAxe = new ItemHuornAxe(itemHuornAxeID, LIVINGWOOD).setUnlocalizedName("axeHuorn").setCreativeTab(HuornTab);
        itemHuornPickaxe = new ItemHuornPickaxe(itemHuornPickaxeID, LIVINGWOOD).setUnlocalizedName("pickaxeHuorn").setCreativeTab(HuornTab);
        itemHuornSpade = new ItemHuornSpade(itemHuornSpadeID, LIVINGWOOD).setUnlocalizedName("spadeHuorn").setCreativeTab(HuornTab);
        itemHuornHoe = new ItemHuornHoe(itemHuornHoeID, LIVINGWOOD).setUnlocalizedName("hoeHuorn").setCreativeTab(HuornTab);
        
        itemPearlSword = new ItemHuornSword(itemPearlSwordID, PEARL).setUnlocalizedName("swordPearl").setCreativeTab(HuornTab);
        itemPearlAxe = new ItemHuornAxe(itemPearlAxeID, PEARL).setUnlocalizedName("axePearl").setCreativeTab(HuornTab);
        itemPearlPickaxe = new ItemHuornPickaxe(itemPearlPickaxeID, PEARL).setUnlocalizedName("pickaxePearl").setCreativeTab(HuornTab);
        itemPearlSpade = new ItemHuornSpade(itemPearlSpadeID, PEARL).setUnlocalizedName("spadePearl").setCreativeTab(HuornTab);
        itemPearlHoe = new ItemHuornHoe(itemPearlHoeID, PEARL).setUnlocalizedName("hoePearl").setCreativeTab(HuornTab);
        
        itemGolemSword = new ItemHuornSword(itemGolemSwordID, LIVINGMETAL).setUnlocalizedName("swordGolem").setCreativeTab(HuornTab);
        itemGolemAxe = new ItemHuornAxe(itemGolemAxeID, LIVINGMETAL).setUnlocalizedName("axeGolem").setCreativeTab(HuornTab);
        itemGolemPickaxe = new ItemHuornPickaxe(itemGolemPickaxeID, LIVINGMETAL).setUnlocalizedName("pickaxeGolem").setCreativeTab(HuornTab);
        itemGolemSpade = new ItemHuornSpade(itemGolemSpadeID, LIVINGMETAL).setUnlocalizedName("spadeGolem").setCreativeTab(HuornTab);
        itemGolemHoe = new ItemHuornHoe(itemGolemHoeID, LIVINGMETAL).setUnlocalizedName("hoeGolem").setCreativeTab(HuornTab);
        
        itemSteelSword = new ItemHuornSword(itemSteelSwordID, STEEL).setUnlocalizedName("swordSteel").setCreativeTab(HuornTab);
        itemSteelAxe = new ItemHuornAxe(itemSteelAxeID, STEEL).setUnlocalizedName("axeSteel").setCreativeTab(HuornTab);
        itemSteelPickaxe = new ItemHuornPickaxe(itemSteelPickaxeID, STEEL).setUnlocalizedName("pickaxeSteel").setCreativeTab(HuornTab);
        itemSteelSpade = new ItemHuornSpade(itemSteelSpadeID, STEEL).setUnlocalizedName("spadeSteel").setCreativeTab(HuornTab);
        itemSteelHoe = new ItemHuornHoe(itemSteelHoeID, STEEL).setUnlocalizedName("hoeSteel").setCreativeTab(HuornTab);
        
        itemMagmaSword = new ItemHuornSword(itemMagmaSwordID, MAGMA).setUnlocalizedName("swordMagma").setCreativeTab(HuornTab);
        itemMagmaAxe = new ItemHuornAxe(itemMagmaAxeID, MAGMA).setUnlocalizedName("axeMagma").setCreativeTab(HuornTab);
        itemMagmaPickaxe = new ItemHuornPickaxe(itemMagmaPickaxeID, MAGMA).setUnlocalizedName("pickaxeMagma").setCreativeTab(HuornTab);
        itemMagmaSpade = new ItemHuornSpade(itemMagmaSpadeID, MAGMA).setUnlocalizedName("spadeMagma").setCreativeTab(HuornTab);
        itemMagmaHoe = new ItemHuornHoe(itemMagmaHoeID, MAGMA).setUnlocalizedName("hoeMagma").setCreativeTab(HuornTab);
        
        itemEmeraldSword = new ItemHuornSword(itemEmeraldSwordID, RUBY).setUnlocalizedName("swordEmerald").setCreativeTab(HuornTab);
        itemEmeraldAxe = new ItemHuornAxe(itemEmeraldAxeID, RUBY).setUnlocalizedName("axeEmerald").setCreativeTab(HuornTab);
        itemEmeraldPickaxe = new ItemHuornPickaxe(itemEmeraldPickaxeID, RUBY).setUnlocalizedName("pickaxeEmerald").setCreativeTab(HuornTab);
        itemEmeraldSpade = new ItemHuornSpade(itemEmeraldSpadeID, RUBY).setUnlocalizedName("spadeEmerald").setCreativeTab(HuornTab);
        itemEmeraldHoe = new ItemHuornHoe(itemEmeraldHoeID, RUBY).setUnlocalizedName("hoeEmerald").setCreativeTab(HuornTab);
        
        itemAmethystSword = new ItemHuornSword(itemAmethystSwordID, AMETHYST).setUnlocalizedName("swordAmethyst").setCreativeTab(HuornTab);
        itemAmethystAxe = new ItemHuornAxe(itemAmethystAxeID, AMETHYST).setUnlocalizedName("axeAmethyst").setCreativeTab(HuornTab);
        itemAmethystPickaxe = new ItemHuornPickaxe(itemAmethystPickaxeID, AMETHYST).setUnlocalizedName("pickaxeAmethyst").setCreativeTab(HuornTab);
        itemAmethystSpade = new ItemHuornSpade(itemAmethystSpadeID, AMETHYST).setUnlocalizedName("spadeAmethyst").setCreativeTab(HuornTab);
        itemAmethystHoe = new ItemHuornHoe(itemAmethystHoeID, AMETHYST).setUnlocalizedName("hoeAmethyst").setCreativeTab(HuornTab);
        
        itemObsidianSword = new ItemHuornSword(itemObsidianSwordID, OBSIDIAN).setUnlocalizedName("swordObsidian").setCreativeTab(HuornTab);
        itemObsidianAxe = new ItemHuornAxe(itemObsidianAxeID, OBSIDIAN).setUnlocalizedName("axeObsidian").setCreativeTab(HuornTab);
        itemObsidianPickaxe = new ItemHuornPickaxe(itemObsidianPickaxeID, OBSIDIAN).setUnlocalizedName("pickaxeObsidian").setCreativeTab(HuornTab);
        itemObsidianSpade = new ItemHuornSpade(itemObsidianSpadeID, OBSIDIAN).setUnlocalizedName("spadeObsidian").setCreativeTab(HuornTab);
        itemObsidianHoe = new ItemHuornHoe(itemObsidianHoeID, OBSIDIAN).setUnlocalizedName("hoeObsidian").setCreativeTab(HuornTab);
        
        /**
         * ARMOR DEFINITIONS
         */
        itemCopperHelmet = new ItemHuornArmor(itemCopperHelmetID, aCOPPER, ModLoader.addArmor("Copper"), 0).setArmorType("copper").setCreativeTab(HuornTab);
        itemCopperPlate = new ItemHuornArmor(itemCopperPlateID, aCOPPER, ModLoader.addArmor("Copper"), 1).setArmorType("copper").setCreativeTab(HuornTab);
        itemCopperLegs = new ItemHuornArmor(itemCopperLegsID, aCOPPER, ModLoader.addArmor("Copper"), 2).setArmorType("copper").setCreativeTab(HuornTab);
        itemCopperBoots = new ItemHuornArmor(itemCopperBootsID, aCOPPER, ModLoader.addArmor("Copper"), 3).setArmorType("copper").setCreativeTab(HuornTab);

        itemEmeraldHelmet = new ItemHuornArmor(itemEmeraldHelmetID, aRUBY, ModLoader.addArmor("Emerald"), 0).setArmorType("emerald").setCreativeTab(HuornTab);
        itemEmeraldPlate = new ItemHuornArmor(itemEmeraldPlateID, aRUBY, ModLoader.addArmor("Emerald"), 1).setArmorType("emerald").setCreativeTab(HuornTab);
        itemEmeraldLegs = new ItemHuornArmor(itemEmeraldLegsID, aRUBY, ModLoader.addArmor("Emerald"), 2).setArmorType("emerald").setCreativeTab(HuornTab);
        itemEmeraldBoots = new ItemHuornArmor(itemEmeraldBootsID, aRUBY, ModLoader.addArmor("Emerald"), 3).setArmorType("emerald").setCreativeTab(HuornTab);

        itemObsidianHelmet = new ItemHuornArmor(itemObsidianHelmetID, aOBSIDIAN, ModLoader.addArmor("Obsidian"), 0).setArmorType("obsidian").setCreativeTab(HuornTab);
        itemObsidianPlate = new ItemHuornArmor(itemObsidianPlateID, aOBSIDIAN, ModLoader.addArmor("Obsidian"), 1).setArmorType("obsidian").setCreativeTab(HuornTab);
        itemObsidianLegs = new ItemHuornArmor(itemObsidianLegsID, aOBSIDIAN, ModLoader.addArmor("Obsidian"), 2).setArmorType("obsidian").setCreativeTab(HuornTab);
        itemObsidianBoots = new ItemHuornArmor(itemObsidianBootsID, aOBSIDIAN, ModLoader.addArmor("Obsidian"), 3).setArmorType("obsidian").setCreativeTab(HuornTab);
        
        debug("Blocks and items declared!");
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        GameRegistry.registerBlock(blockHuornLog, blockHuornLog.getUnlocalizedName());
        GameRegistry.registerBlock(blockHuornWood, blockHuornWood.getUnlocalizedName());
        GameRegistry.registerBlock(blockHuornDoubleSlab, blockHuornDoubleSlab.getUnlocalizedName());
        GameRegistry.registerBlock(blockHuornSingleSlab, blockHuornSingleSlab.getUnlocalizedName());
        GameRegistry.registerBlock(blockHuornStairs, blockHuornStairs.getUnlocalizedName());
        GameRegistry.registerBlock(blockHuornSapling, blockHuornSapling.getUnlocalizedName());
        GameRegistry.registerBlock(blockHumus, blockHumus.getUnlocalizedName());
        GameRegistry.registerBlock(blockHuornLeaves, blockHuornLeaves.getUnlocalizedName());
        GameRegistry.registerBlock(blockBlueQuartz, blockBlueQuartz.getUnlocalizedName());
        GameRegistry.registerBlock(blockClearWater, blockClearWater.getUnlocalizedName());
        //GameRegistry.registerBlock(blockCondensatorOff, blockCondensatorOff.getUnlocalizedName());
        //GameRegistry.registerBlock(blockCondensatorOn, blockCondensatorOn.getUnlocalizedName());
        GameRegistry.registerBlock(blockObsidianOre, blockObsidianOre.getUnlocalizedName());
        GameRegistry.registerBlock(blockAmethystOre, blockAmethystOre.getUnlocalizedName());
        GameRegistry.registerBlock(blockAmethyst, blockAmethyst.getUnlocalizedName());
        //GameRegistry.registerBlock(blockTreetap, blockTreetap.getUnlocalizedName());
        GameRegistry.registerBlock(blockCopperOre, blockCopperOre.getUnlocalizedName());
        GameRegistry.registerBlock(blockCopper, blockCopper.getUnlocalizedName());
        //GameRegistry.registerBlock(blockQuartzLight, blockQuartzLight.getUnlocalizedName());
        
        GameRegistry.registerItem(itemHuornSap, itemHuornSap.getUnlocalizedName());
        GameRegistry.registerItem(itemHuornAshes, itemHuornAshes.getUnlocalizedName());
        GameRegistry.registerItem(itemHuornLeaf, itemHuornLeaf.getUnlocalizedName());
        GameRegistry.registerItem(itemClearWaterBucket, itemClearWaterBucket.getUnlocalizedName());
        GameRegistry.registerItem(itemHuornPotion, itemHuornPotion.getUnlocalizedName());
        GameRegistry.registerItem(itemHuornSplashPotion, itemHuornSplashPotion.getUnlocalizedName());
        GameRegistry.registerItem(itemFairyBottle, itemFairyBottle.getUnlocalizedName());
        /**
         * TOOLS REGISTRATIONS
         */
        GameRegistry.registerItem(itemCopperSword, itemCopperSword.getUnlocalizedName());
        GameRegistry.registerItem(itemCopperAxe, itemCopperAxe.getUnlocalizedName());
        GameRegistry.registerItem(itemCopperPickaxe, itemCopperPickaxe.getUnlocalizedName());
        GameRegistry.registerItem(itemCopperSpade, itemCopperSpade.getUnlocalizedName());
        GameRegistry.registerItem(itemCopperHoe, itemCopperHoe.getUnlocalizedName());
        
        GameRegistry.registerItem(itemHuornSword, itemHuornSword.getUnlocalizedName());
        GameRegistry.registerItem(itemHuornAxe, itemHuornAxe.getUnlocalizedName());
        GameRegistry.registerItem(itemHuornPickaxe, itemHuornPickaxe.getUnlocalizedName());
        GameRegistry.registerItem(itemHuornSpade, itemHuornSpade.getUnlocalizedName());
        GameRegistry.registerItem(itemHuornHoe, itemHuornHoe.getUnlocalizedName());
        
        GameRegistry.registerItem(itemPearlSword, itemPearlSword.getUnlocalizedName());
        GameRegistry.registerItem(itemPearlAxe, itemPearlAxe.getUnlocalizedName());
        GameRegistry.registerItem(itemPearlPickaxe, itemPearlPickaxe.getUnlocalizedName());
        GameRegistry.registerItem(itemPearlSpade, itemPearlSpade.getUnlocalizedName());
        GameRegistry.registerItem(itemPearlHoe, itemPearlHoe.getUnlocalizedName());
        
        GameRegistry.registerItem(itemGolemSword, itemGolemSword.getUnlocalizedName());
        GameRegistry.registerItem(itemGolemAxe, itemGolemAxe.getUnlocalizedName());
        GameRegistry.registerItem(itemGolemPickaxe, itemGolemPickaxe.getUnlocalizedName());
        GameRegistry.registerItem(itemGolemSpade, itemGolemSpade.getUnlocalizedName());
        GameRegistry.registerItem(itemGolemHoe, itemGolemHoe.getUnlocalizedName());
        
        GameRegistry.registerItem(itemSteelSword, itemSteelSword.getUnlocalizedName());
        GameRegistry.registerItem(itemSteelAxe, itemSteelAxe.getUnlocalizedName());
        GameRegistry.registerItem(itemSteelPickaxe, itemSteelPickaxe.getUnlocalizedName());
        GameRegistry.registerItem(itemSteelSpade, itemSteelSpade.getUnlocalizedName());
        GameRegistry.registerItem(itemSteelHoe, itemSteelHoe.getUnlocalizedName());
        
        GameRegistry.registerItem(itemMagmaSword, itemMagmaSword.getUnlocalizedName());
        GameRegistry.registerItem(itemMagmaAxe, itemMagmaAxe.getUnlocalizedName());
        GameRegistry.registerItem(itemMagmaPickaxe, itemMagmaPickaxe.getUnlocalizedName());
        GameRegistry.registerItem(itemMagmaSpade, itemMagmaSpade.getUnlocalizedName());
        GameRegistry.registerItem(itemMagmaHoe, itemMagmaHoe.getUnlocalizedName());
        
        GameRegistry.registerItem(itemEmeraldSword, itemEmeraldSword.getUnlocalizedName());
        GameRegistry.registerItem(itemEmeraldAxe, itemEmeraldAxe.getUnlocalizedName());
        GameRegistry.registerItem(itemEmeraldPickaxe, itemEmeraldPickaxe.getUnlocalizedName());
        GameRegistry.registerItem(itemEmeraldSpade, itemEmeraldSpade.getUnlocalizedName());
        GameRegistry.registerItem(itemEmeraldHoe, itemEmeraldHoe.getUnlocalizedName());
        
        GameRegistry.registerItem(itemAmethystSword, itemAmethystSword.getUnlocalizedName());
        GameRegistry.registerItem(itemAmethystAxe, itemAmethystAxe.getUnlocalizedName());
        GameRegistry.registerItem(itemAmethystPickaxe, itemAmethystPickaxe.getUnlocalizedName());
        GameRegistry.registerItem(itemAmethystSpade, itemAmethystSpade.getUnlocalizedName());
        GameRegistry.registerItem(itemAmethystHoe, itemAmethystHoe.getUnlocalizedName());
        
        GameRegistry.registerItem(itemObsidianSword, itemObsidianSword.getUnlocalizedName());
        GameRegistry.registerItem(itemObsidianAxe, itemObsidianAxe.getUnlocalizedName());
        GameRegistry.registerItem(itemObsidianPickaxe, itemObsidianPickaxe.getUnlocalizedName());
        GameRegistry.registerItem(itemObsidianSpade, itemObsidianSpade.getUnlocalizedName());
        GameRegistry.registerItem(itemObsidianHoe, itemObsidianHoe.getUnlocalizedName());
        
        /**
         * ARMOR REGISTRATIONS
         */
        GameRegistry.registerItem(itemCopperHelmet, itemCopperHelmet.getUnlocalizedName());
        GameRegistry.registerItem(itemCopperPlate, itemCopperPlate.getUnlocalizedName());
        GameRegistry.registerItem(itemCopperLegs, itemCopperLegs.getUnlocalizedName());
        GameRegistry.registerItem(itemCopperBoots, itemCopperBoots.getUnlocalizedName());

        GameRegistry.registerItem(itemEmeraldHelmet, itemEmeraldHelmet.getUnlocalizedName());
        GameRegistry.registerItem(itemEmeraldPlate, itemEmeraldPlate.getUnlocalizedName());
        GameRegistry.registerItem(itemEmeraldLegs, itemEmeraldLegs.getUnlocalizedName());
        GameRegistry.registerItem(itemEmeraldBoots, itemEmeraldBoots.getUnlocalizedName());

        GameRegistry.registerItem(itemObsidianHelmet, itemObsidianHelmet.getUnlocalizedName());
        GameRegistry.registerItem(itemObsidianPlate, itemObsidianPlate.getUnlocalizedName());
        GameRegistry.registerItem(itemObsidianLegs, itemObsidianLegs.getUnlocalizedName());
        GameRegistry.registerItem(itemObsidianBoots, itemObsidianBoots.getUnlocalizedName());
        
        GameRegistry.registerItem(itemObsidian, itemObsidian.getUnlocalizedName());
        GameRegistry.registerItem(itemPearl, itemPearl.getUnlocalizedName());
        GameRegistry.registerItem(itemAmethyst, itemAmethyst.getUnlocalizedName());
        GameRegistry.registerItem(itemGolemIngot, itemGolemIngot.getUnlocalizedName());
        GameRegistry.registerItem(itemMagmaIngot, itemMagmaIngot.getUnlocalizedName());
        GameRegistry.registerItem(itemSteelIngot, itemSteelIngot.getUnlocalizedName());
        GameRegistry.registerItem(itemCopperIngot, itemCopperIngot.getUnlocalizedName());
        //GameRegistry.registerItem(itemTreetap, itemTreetap.getUnlocalizedName());
		GameRegistry.registerWorldGenerator(new WorldGenGlobal());
		GameRegistry.registerFuelHandler(new FuelHandler());
		//GameRegistry.registerTileEntity(TileEntityBrewing.class, "tileEntityCondensator");
		//GameRegistry.registerTileEntity(TileEntityTreetap.class, "tileEntityTreetap");
		EntityRegistry.registerModEntity(EntityGrenade.class, "entityGrenade", 105, this, 350, 10, true);
		EntityRegistry.registerModEntity(EntityFairyMob.class, "entityFairy", 106, this, 350, 10, true);
		FluidContainerRegistry.registerFluidContainer(fluidClearWater, new ItemStack(itemClearWaterBucket), new ItemStack(Item.bucketEmpty));
		NetworkRegistry.instance().registerGuiHandler(instance, guihandler);
        MinecraftForge.setBlockHarvestLevel(blockHuornLog, "axe", 0);
        MinecraftForge.setBlockHarvestLevel(blockHuornWood, "axe", 0);
        MinecraftForge.setBlockHarvestLevel(blockHuornDoubleSlab, "axe", 0);
        MinecraftForge.setBlockHarvestLevel(blockHuornSingleSlab, "axe", 0);
        MinecraftForge.setBlockHarvestLevel(blockHuornStairs, "axe", 0);
        MinecraftForge.setBlockHarvestLevel(blockHumus, "spade", 0);
        MinecraftForge.setBlockHarvestLevel(blockObsidianOre, "pickaxe", 2);
        MinecraftForge.setBlockHarvestLevel(blockAmethystOre, "pickaxe", 3);
        MinecraftForge.setBlockHarvestLevel(blockAmethyst, "pickaxe", 3);
        MinecraftForge.setBlockHarvestLevel(blockCopperOre, "pickaxe", 1);
        MinecraftForge.setBlockHarvestLevel(blockCopper, "pickaxe", 1);
		MinecraftForge.EVENT_BUS.register(new HuornPotionHook());
		MinecraftForge.EVENT_BUS.register(new ToolsHook());
		MinecraftForge.EVENT_BUS.register(new ArmorHook());
		MinecraftForge.EVENT_BUS.register(new HuornBoneMealEvent());
		MinecraftForge.EVENT_BUS.register(new ClearWaterBucketEvent());
		// Normal recipes
        GameRegistry.addShapelessRecipe(new ItemStack(blockHuornWood, 4), new ItemStack(blockHuornLog));
        GameRegistry.addShapelessRecipe(new ItemStack(Block.woodenButton), new ItemStack(blockHuornWood));
        GameRegistry.addRecipe(new ItemStack(Item.stick, 4), "x", "x", 'x', new ItemStack(blockHuornWood));
        GameRegistry.addRecipe(new ItemStack(Block.pressurePlatePlanks), "xx", 'x', new ItemStack(blockHuornWood));
        GameRegistry.addRecipe(new ItemStack(Item.bowlEmpty, 4), "xyx", "yxy", 'x', new ItemStack(blockHuornWood));
        GameRegistry.addRecipe(new ItemStack(blockHuornSingleSlab, 6), "xxx", 'x', new ItemStack(blockHuornWood));
        GameRegistry.addRecipe(new ItemStack(Block.workbench), "xx", "xx", 'x', new ItemStack(blockHuornWood));
        GameRegistry.addRecipe(new ItemStack(Item.boat), "xyx", "xxx", 'x', new ItemStack(blockHuornWood));
        GameRegistry.addRecipe(new ItemStack(Item.doorWood), "xx", "xx", "xx", 'x', new ItemStack(blockHuornWood));
        GameRegistry.addRecipe(new ItemStack(Block.trapdoor, 2), "xxx", "xxx", 'x', new ItemStack(blockHuornWood));
        GameRegistry.addRecipe(new ItemStack(blockHuornStairs, 4), "yyx", "yxx", "xxx", 'x', new ItemStack(blockHuornWood));
        GameRegistry.addRecipe(new ItemStack(Block.chest), "xxx", "xyx", "xxx", 'x', new ItemStack(blockHuornWood));
        // Combined recipes
        GameRegistry.addRecipe(new ItemStack(Block.fenceGate), "xyx", "xyx", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(Item.sign, 3), "xxx", "xxx", "zyz", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(Item.bed), "yyy", "xxx", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Block.cloth));
        GameRegistry.addRecipe(new ItemStack(Block.music), "xxx", "xyx", "xxx", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Item.redstone));
        GameRegistry.addRecipe(new ItemStack(Block.jukebox), "xxx", "xyx", "xxx", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Item.diamond));
        GameRegistry.addRecipe(new ItemStack(Block.pistonBase), "xxx", "yzy", "ywy", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Block.cobblestone), 'z', new ItemStack(Item.ingotIron), 'w', new ItemStack(Item.redstone));
        GameRegistry.addRecipe(new ItemStack(Block.bookShelf), "xxx", "yyy", "xxx", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Item.book));
        // Custom recipes
        GameRegistry.addRecipe(new ItemStack(itemHuornSap), "x", "y", 'x', new ItemStack(Item.bowlEmpty), 'y', new ItemStack(blockHuornLog));
        GameRegistry.addShapelessRecipe(new ItemStack(itemAmethyst, 8, 1), new ItemStack(itemAmethyst, 1, 0));
        GameRegistry.addShapelessRecipe(new ItemStack(itemAmethyst, 8, 2), new ItemStack(itemAmethyst, 1, 1));
        GameRegistry.addRecipe(new ItemStack(Block.obsidian), "xxx", "xxx", "xxx", 'x', new ItemStack(itemObsidian, 1, 0));
        GameRegistry.addRecipe(new ItemStack(itemObsidian, 1, 0), "xxx", "xxx", "xxx", 'x', new ItemStack(itemObsidian, 1, 1));
        GameRegistry.addRecipe(new ItemStack(blockAmethyst), "xxx", "xxx", "xxx", 'x', new ItemStack(itemAmethyst, 1, 0));
        GameRegistry.addShapelessRecipe(new ItemStack(itemAmethyst, 1, 9), new ItemStack(blockAmethyst));
        //GameRegistry.addRecipe(new ItemStack(itemTreetap), "yxx", "xyy", 'x', new ItemStack(Item.stick, 1, 0));
        GameRegistry.addRecipe(new ItemStack(blockCopper), "xxx", "xxx", "xxx", 'x', new ItemStack(itemCopperIngot, 1, 0));
        GameRegistry.addShapelessRecipe(new ItemStack(itemMagmaIngot), new ItemStack(Item.netherrackBrick), new ItemStack(Item.magmaCream));
        GameRegistry.addShapelessRecipe(new ItemStack(itemSteelIngot), new ItemStack(Item.ingotIron), new ItemStack(itemCopperIngot));
        /**
         * TOOLS RECIPES
         */
        GameRegistry.addRecipe(new ItemStack(itemCopperSword), "x", "x", "y", 'x', new ItemStack(itemCopperIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemCopperAxe), "xx", "xy", "zy", 'x', new ItemStack(itemCopperIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemCopperPickaxe), "xxx", "zyz", "zyz", 'x', new ItemStack(itemCopperIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemCopperSpade), "x", "y", "y", 'x', new ItemStack(itemCopperIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemCopperHoe), "xx", "zy", "zy", 'x', new ItemStack(itemCopperIngot), 'y', new ItemStack(Item.stick));
        
        GameRegistry.addRecipe(new ItemStack(itemHuornSword), "x", "x", "y", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemHuornAxe), "xx", "xy", "zy", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemHuornPickaxe), "xxx", "zyz", "zyz", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemHuornSpade), "x", "y", "y", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemHuornHoe), "xx", "zy", "zy", 'x', new ItemStack(blockHuornWood), 'y', new ItemStack(Item.stick));
        
        GameRegistry.addRecipe(new ItemStack(itemPearlSword), "x", "x", "y", 'x', new ItemStack(itemPearl), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemPearlAxe), "xx", "xy", "zy", 'x', new ItemStack(itemPearl), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemPearlPickaxe), "xxx", "zyz", "zyz", 'x', new ItemStack(itemPearl), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemPearlSpade), "x", "y", "y", 'x', new ItemStack(itemPearl), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemPearlHoe), "xx", "zy", "zy", 'x', new ItemStack(itemPearl), 'y', new ItemStack(Item.stick));

        GameRegistry.addRecipe(new ItemStack(itemGolemSword), "x", "x", "y", 'x', new ItemStack(itemGolemIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemGolemAxe), "xx", "xy", "zy", 'x', new ItemStack(itemGolemIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemGolemPickaxe), "xxx", "zyz", "zyz", 'x', new ItemStack(itemGolemIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemGolemSpade), "x", "y", "y", 'x', new ItemStack(itemGolemIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemGolemHoe), "xx", "zy", "zy", 'x', new ItemStack(itemGolemIngot), 'y', new ItemStack(Item.stick));

        GameRegistry.addRecipe(new ItemStack(itemSteelSword), "x", "x", "y", 'x', new ItemStack(itemSteelIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemSteelAxe), "xx", "xy", "zy", 'x', new ItemStack(itemSteelIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemSteelPickaxe), "xxx", "zyz", "zyz", 'x', new ItemStack(itemSteelIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemSteelSpade), "x", "y", "y", 'x', new ItemStack(itemSteelIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemSteelHoe), "xx", "zy", "zy", 'x', new ItemStack(itemSteelIngot), 'y', new ItemStack(Item.stick));

        GameRegistry.addRecipe(new ItemStack(itemMagmaSword), "x", "x", "y", 'x', new ItemStack(itemMagmaIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemMagmaAxe), "xx", "xy", "zy", 'x', new ItemStack(itemMagmaIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemMagmaPickaxe), "xxx", "zyz", "zyz", 'x', new ItemStack(itemMagmaIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemMagmaSpade), "x", "y", "y", 'x', new ItemStack(itemMagmaIngot), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemMagmaHoe), "xx", "zy", "zy", 'x', new ItemStack(itemMagmaIngot), 'y', new ItemStack(Item.stick));
        
        GameRegistry.addRecipe(new ItemStack(itemEmeraldSword), "x", "x", "y", 'x', new ItemStack(Item.emerald, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemEmeraldAxe), "xx", "xy", "zy", 'x', new ItemStack(Item.emerald, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemEmeraldPickaxe), "xxx", "zyz", "zyz", 'x', new ItemStack(Item.emerald, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemEmeraldSpade), "x", "y", "y", 'x', new ItemStack(Item.emerald, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemEmeraldHoe), "xx", "zy", "zy", 'x', new ItemStack(Item.emerald, 1, 0), 'y', new ItemStack(Item.stick));
        
        GameRegistry.addRecipe(new ItemStack(itemAmethystSword), "x", "x", "y", 'x', new ItemStack(itemAmethyst, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemAmethystAxe), "xx", "xy", "zy", 'x', new ItemStack(itemAmethyst, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemAmethystPickaxe), "xxx", "zyz", "zyz", 'x', new ItemStack(itemAmethyst, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemAmethystSpade), "x", "y", "y", 'x', new ItemStack(itemAmethyst, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemAmethystHoe), "xx", "zy", "zy", 'x', new ItemStack(itemAmethyst, 1, 0), 'y', new ItemStack(Item.stick));

        GameRegistry.addRecipe(new ItemStack(itemObsidianSword), "x", "x", "y", 'x', new ItemStack(itemObsidian, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemObsidianAxe), "xx", "xy", "zy", 'x', new ItemStack(itemObsidian, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemObsidianPickaxe), "xxx", "zyz", "zyz", 'x', new ItemStack(itemObsidian, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemObsidianSpade), "x", "y", "y", 'x', new ItemStack(itemObsidian, 1, 0), 'y', new ItemStack(Item.stick));
        GameRegistry.addRecipe(new ItemStack(itemObsidianHoe), "xx", "zy", "zy", 'x', new ItemStack(itemObsidian, 1, 0), 'y', new ItemStack(Item.stick));
        
		/**
		 * ARMOR RECIPES
		 */
        GameRegistry.addRecipe(new ItemStack(itemCopperHelmet), "xxx", "xyx", 'x', new ItemStack(itemCopperIngot));
        GameRegistry.addRecipe(new ItemStack(itemCopperPlate), "xyx", "xxx", "xxx", 'x', new ItemStack(itemCopperIngot));
        GameRegistry.addRecipe(new ItemStack(itemCopperLegs), "xxx", "xyx", "xyx", 'x', new ItemStack(itemCopperIngot));
        GameRegistry.addRecipe(new ItemStack(itemCopperBoots), "xyx", "xyx", 'x', new ItemStack(itemCopperIngot));

        GameRegistry.addRecipe(new ItemStack(itemEmeraldHelmet), "xxx", "xyx", 'x', new ItemStack(Item.emerald));
        GameRegistry.addRecipe(new ItemStack(itemEmeraldPlate), "xyx", "xxx", "xxx", 'x', new ItemStack(Item.emerald));
        GameRegistry.addRecipe(new ItemStack(itemEmeraldLegs), "xxx", "xyx", "xyx", 'x', new ItemStack(Item.emerald));
        GameRegistry.addRecipe(new ItemStack(itemEmeraldBoots), "xyx", "xyx", 'x', new ItemStack(Item.emerald));

        GameRegistry.addRecipe(new ItemStack(itemObsidianHelmet), "xxx", "xyx", 'x', new ItemStack(itemObsidian, 1, 1));
        GameRegistry.addRecipe(new ItemStack(itemObsidianPlate), "xyx", "xxx", "xxx", 'x', new ItemStack(itemCopperIngot, 1, 1));
        GameRegistry.addRecipe(new ItemStack(itemObsidianLegs), "xxx", "xyx", "xyx", 'x', new ItemStack(itemCopperIngot, 1, 1));
        GameRegistry.addRecipe(new ItemStack(itemObsidianBoots), "xyx", "xyx", 'x', new ItemStack(itemCopperIngot, 1, 1));
        
        // Smelting
        GameRegistry.addSmelting(blockHuornLog.blockID, new ItemStack(itemHuornAshes, 16), 16);
        GameRegistry.addSmelting(blockHuornWood.blockID, new ItemStack(itemHuornAshes, 4), 4);
        GameRegistry.addSmelting(blockHuornDoubleSlab.blockID, new ItemStack(itemHuornAshes, 4), 4);
        GameRegistry.addSmelting(blockHuornSingleSlab.blockID, new ItemStack(itemHuornAshes, 2), 2);
        GameRegistry.addSmelting(blockHuornStairs.blockID, new ItemStack(itemHuornAshes, 3), 3);
        GameRegistry.addSmelting(blockObsidianOre.blockID, new ItemStack(itemObsidian, 1, 0), 24);
        GameRegistry.addSmelting(blockAmethystOre.blockID, new ItemStack(itemAmethyst, 2, 1), 48);
        GameRegistry.addSmelting(blockCopperOre.blockID, new ItemStack(itemCopperIngot, 1, 1), 8);
        Item.itemsList[blockHuornSingleSlab.blockID] = (new ItemSlab(blockHuornSingleSlab.blockID - 256,
				(BlockHalfSlab)blockHuornSingleSlab,
				(BlockHalfSlab)blockHuornDoubleSlab,
				false));
        LanguageRegistry.instance().loadLocalization("huornforest:lang/en_US.lang", "en_US", false);
        LanguageRegistry.instance().loadLocalization("huornforest:lang/fr_CA.lang", "fr_CA", false);
        proxy.registerRenderers();
        debug("Mod registered!");
    }

    @EventHandler
    public void serverLoad(FMLServerStartingEvent event) {
    	if (canUseCommands) {
    		event.registerServerCommand(new CommandSpawnIsland());
    		event.registerServerCommand(new CommandTeleportDimension());
    		debug("Commands activated!");
    	}
    }

	public static void debug(Object arg0) {
		log(arg0, false, Level.INFO);
	}
	
    public static void debug(Object arg0, Level arg1) {
    	log(arg0, false, arg1);
    }
    
    public static void log(Object arg0) {
    	log(arg0, true, Level.INFO);
    }

    public static void log(Object arg0, Level arg1) {
    	log(arg0, true, arg1);
    }

    public static void log(Object arg0, boolean arg1, Level arg2) {
    	if (verbose || arg1 || arg2 == Level.SEVERE) {
    		LOGGER.log(arg2, (String)arg0);
    	} 
    }
}