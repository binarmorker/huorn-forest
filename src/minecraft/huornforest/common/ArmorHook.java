package huornforest.common;

import java.util.List;
import java.util.Random;

import huornforest.items.ItemHuornArmor;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.world.WorldEvent;

public class ArmorHook
{
	public enum Effect {
	    CONDUCTOR, MAGNET, OBSIDIAN
	}
	
	public static Effect getArmorEffect(EnumArmorMaterial material) {
		if(material == HuornForest.aCOPPER) {
			return Effect.CONDUCTOR;
		} else if(material == HuornForest.aRUBY) {
			return Effect.MAGNET;
		} else if(material == HuornForest.aOBSIDIAN) {
			return Effect.OBSIDIAN;
		} else {
			return null;
		}
	}
	
	public static EnumArmorMaterial hasFullSet(EntityPlayer player) {

		ItemArmor[] currentArmor = new ItemArmor[4];
		EnumArmorMaterial prevMat = null;
		EnumArmorMaterial material = null;
		boolean fullSet = true;
		
		for (int i = 0; i < 4; i++) {
			if (player.inventory.armorInventory[i] != null && player.inventory.armorInventory[i].getItem() instanceof ItemArmor) {
				currentArmor[i] = (ItemArmor)(player.inventory.armorInventory[i].getItem());
				material = currentArmor[i].getArmorMaterial();
				
				if (prevMat == null) {
					prevMat = currentArmor[i].getArmorMaterial();
				}
				
				if (material != prevMat) {
					fullSet = false;
				}
				
				prevMat = material;
			} else {
				fullSet = false;
			}
		}
		
		if (fullSet) {
			return material;
		} else {
			return null;
		}
		
	}
	
	public static void onBadWeather(World world, EntityPlayer player) {
		EnumArmorMaterial material = hasFullSet((EntityPlayer)player);
		if (material != null && material == HuornForest.aCOPPER) {
			EntityLightningBolt bolt = new EntityLightningBolt(world, Math.round(player.posX), world.getFirstUncoveredBlock((int)Math.round(player.posX), (int)Math.round(player.posZ)), Math.round(player.posZ));
			world.addWeatherEffect(bolt);
		}
	}

	@ForgeSubscribe
	public void onEntityHurt(LivingAttackEvent event) {
		DamageSource source = event.source;
		Entity entity = event.entity;
		
		if (entity instanceof EntityPlayer) {
			EntityPlayerMP player = (EntityPlayerMP)entity;
			EnumArmorMaterial material = hasFullSet(player);
			if (material != null && material == HuornForest.aOBSIDIAN) {
				if (source == DamageSource.inFire || source == DamageSource.onFire || source == DamageSource.lava) {
					event.setCanceled(true);
					player.extinguish();
				}
			}
		}
	}
}