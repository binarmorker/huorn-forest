package huornforest.common;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.potion.Potion;
import net.minecraft.util.ResourceLocation;

public class PotionHuorn extends Potion
{
	private boolean customSkin;
	
    public PotionHuorn(int par1, boolean par2, int par3)
    {
        super(par1, par2, par3);
    }
	
    public Potion setIconIndex(int par1, int par2)
    {
        this.setIconIndex(par1, par2, true);
        return this;
    }
    
    public Potion setIconIndex(int par1, int par2, boolean isDefault) {
    	super.setIconIndex(par1, par2);
    	customSkin = isDefault;
    	return this;
    }
    
	@Override
	@SideOnly(Side.CLIENT)
	public int getStatusIconIndex()
	{
		if (customSkin) Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("huornforest:textures/gui/HuornForestPotionFX.png"));
		return super.getStatusIconIndex();
	}
	
}