package huornforest.entities;

import java.util.List;

import huornforest.common.HuornForest;
import huornforest.common.HuornPotionHook;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntitySmokeFX;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.item.ItemExpireEvent;

public class EntityGrenade extends EntityThrowable
{
    public int fuse;
    public int mode;
    public int intensity;
    public boolean collide;

    public EntityGrenade(World par1World)
    {
        super(par1World);
    }

    public EntityGrenade(World par1World, EntityPlayer par3EntityPlayer, int meta, int time)
    {
        super(par1World, par3EntityPlayer);
        this.intensity = (meta % 3) + 1;
        time *= this.intensity;
        this.fuse = time;
        this.mode = (int)(meta / 3);
    }
    
    @Override
    public void onUpdate()
    {
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        this.motionY -= 0.03999999910593033D;
        this.noClip = this.pushOutOfBlocks(this.posX, (this.boundingBox.minY + this.boundingBox.maxY) / 2.0D, this.posZ);
        this.moveEntity(this.motionX, this.motionY, this.motionZ);
        boolean flag1 = (int)this.prevPosX != (int)this.posX || (int)this.prevPosY != (int)this.posY || (int)this.prevPosZ != (int)this.posZ;

        if (flag1 || this.ticksExisted % 25 == 0)
        {
            if (this.worldObj.getBlockMaterial(MathHelper.floor_double(this.posX), MathHelper.floor_double(this.posY), MathHelper.floor_double(this.posZ)) == Material.lava)
            {
                this.motionY = 0.20000000298023224D;
                this.motionX = (double)((this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F);
                this.motionZ = (double)((this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F);
                this.playSound("random.fizz", 0.4F, 2.0F + this.rand.nextFloat() * 0.4F);
            }
        }

        float f = 0.98F;

        if (this.onGround)
        {
            f = 0.58800006F;
            int i = this.worldObj.getBlockId(MathHelper.floor_double(this.posX), MathHelper.floor_double(this.boundingBox.minY) - 1, MathHelper.floor_double(this.posZ));

            if (i > 0)
            {
                f = Block.blocksList[i].slipperiness * 0.98F;
            }
        }

        this.motionX *= (double)f;
        this.motionY *= 0.9800000190734863D;
        this.motionZ *= (double)f;
        
        if (this.onGround)
        {
            if (HuornForest.grenadeMode && this.mode != 3) {
            	grenadeExplode();
            }
            this.motionY *= -0.5D;
        }
        
        if (this.fuse-- <= 0)
        {
            if (!HuornForest.grenadeMode || this.mode == 3) {
            	grenadeExplode();
            }
        }
    }
    
    protected void grenadeExplode() {
    	
        if (!this.worldObj.isRemote)
        {
            boolean flag2 = this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing");

            if (this.mode == 0)
            {
                this.worldObj.newExplosion(this, this.posX, this.posY, this.posZ, (2.5F * this.intensity) - 1, false, flag2);
                this.setDead();
            }
            else if (this.mode == 1 || this.mode == 2)
            {
                int repBlock;

                if (this.mode == 1)
                {
                    this.worldObj.newExplosion(this, this.posX, this.posY, this.posZ, this.intensity, true, flag2);
                    repBlock = Block.fire.blockID;
                }
                else
                {
                    this.worldObj.newExplosion(this, this.posX, this.posY, this.posZ, 0, false, false);
                    repBlock = 0;
                }

                int i = MathHelper.floor_double(this.posX);
                int j = MathHelper.floor_double(this.posY);
                int k = MathHelper.floor_double(this.posZ);
                double radius = (1.5D * this.intensity) + 1.5D;
                double radiusSq = radius * radius;
                int ceilRadius = (int)Math.ceil(radius);

                for (int x = 0; x <= ceilRadius; x++)
                {
                    for (int y = 0; y <= ceilRadius; y++)
                    {
                        for (int z = 0; z <= ceilRadius; z++)
                        {
                            double dSq = (x * x + y * y + z * z);

                            if (dSq > radiusSq)
                            {
                                continue;
                            }

                            if (this.worldObj.isAirBlock(i + x, j + y, k + z) ||
                                    (this.mode == 2 && this.worldObj.getBlockId(i + x, j + y, k + z) != Block.bedrock.blockID))
                            {
                                this.worldObj.destroyBlock(i + x, j + y, k + z, true);
                                this.worldObj.setBlock(i + x, j + y, k + z, repBlock);
                            }

                            if (this.worldObj.isAirBlock(i - x, j + y, k + z) ||
                                    (this.mode == 2 && this.worldObj.getBlockId(i - x, j + y, k + z) != Block.bedrock.blockID))
                            {
                                this.worldObj.destroyBlock(i - x, j + y, k + z, true);
                                this.worldObj.setBlock(i - x, j + y, k + z, repBlock);
                            }

                            if (this.worldObj.isAirBlock(i + x, j - y, k + z) ||
                                    (this.mode == 2 && this.worldObj.getBlockId(i + x, j - y, k + z) != Block.bedrock.blockID))
                            {
                                this.worldObj.destroyBlock(i + x, j - y, k + z, true);
                                this.worldObj.setBlock(i + x, j - y, k + z, repBlock);
                            }

                            if (this.worldObj.isAirBlock(i + x, j + y, k - z) ||
                                    (this.mode == 2 && this.worldObj.getBlockId(i + x, j + y, k - z) != Block.bedrock.blockID))
                            {
                                this.worldObj.destroyBlock(i + x, j + y, k - z, true);
                                this.worldObj.setBlock(i + x, j + y, k - z, repBlock);
                            }

                            if (this.worldObj.isAirBlock(i - x, j - y, k + z) ||
                                    (this.mode == 2 && this.worldObj.getBlockId(i - x, j - y, k + z) != Block.bedrock.blockID))
                            {
                                this.worldObj.destroyBlock(i - x, j - y, k + z, true);
                                this.worldObj.setBlock(i - x, j - y, k + z, repBlock);
                            }

                            if (this.worldObj.isAirBlock(i + x, j - y, k - z) ||
                                    (this.mode == 2 && this.worldObj.getBlockId(i + x, j - y, k - z) != Block.bedrock.blockID))
                            {
                                this.worldObj.destroyBlock(i + x, j - y, k - z, true);
                                this.worldObj.setBlock(i + x, j - y, k - z, repBlock);
                            }

                            if (this.worldObj.isAirBlock(i - x, j + y, k - z) ||
                                    (this.mode == 2 && this.worldObj.getBlockId(i - x, j + y, k - z) != Block.bedrock.blockID))
                            {
                                this.worldObj.destroyBlock(i - x, j + y, k - z, true);
                                this.worldObj.setBlock(i - x, j + y, k - z, repBlock);
                            }

                            if (this.worldObj.isAirBlock(i - x, j - y, k - z) ||
                                    (this.mode == 2 && this.worldObj.getBlockId(i - x, j - y, k - z) != Block.bedrock.blockID))
                            {
                                this.worldObj.destroyBlock(i - x, j - y, k - z, true);
                                this.worldObj.setBlock(i - x, j - y, k - z, repBlock);
                            }
                        }
                    }
                }
                this.setDead();
            }
            else if (this.mode == 3) {
            	AxisAlignedBB ents = AxisAlignedBB.getAABBPool().getAABB((double)this.posX, (double)this.posY, (double)this.posZ, (double)(this.posX + 1), (double)(this.posY + 1), (double)(this.posZ + 1)).expand(4D * this.intensity, 4D * this.intensity, 4D * this.intensity);
            	int num = this.worldObj.getEntitiesWithinAABBExcludingEntity(getThrower(), ents).size();
            	Entity[] entities = new Entity[num];
            	this.worldObj.getEntitiesWithinAABBExcludingEntity(getThrower(), ents).toArray(entities);
            	for (int i=0; i<num; i++) {
            		if (entities[i] instanceof EntityLivingBase) {
            			EntityLivingBase living = (EntityLivingBase)entities[i];
            			HuornPotionHook.randomTeleport(living, this.intensity);
            		}
            	}
                this.worldObj.playAuxSFX(2002, (int)Math.round(this.posX), (int)Math.round(this.posY), (int)Math.round(this.posZ), 0);
            	this.setDead();
    		}
        }
    }

	@Override
	protected void onImpact(MovingObjectPosition movingobjectposition) {}
	
    protected float getGravityVelocity()
    {
        return 0.06F;
    }

    public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound) {}

    public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound) {}
}
