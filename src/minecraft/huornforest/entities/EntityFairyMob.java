package huornforest.entities;

import huornforest.common.HuornForest;

import java.lang.reflect.Field;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundManager;
import net.minecraft.client.audio.SoundPool;
import net.minecraft.client.audio.SoundPoolEntry;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityFlying;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.EntityDragonPart;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityLargeFireball;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.pathfinding.PathEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityFairyMob extends EntityLiving
{
	//public boolean hasNotified;
    /*public int courseChangeCooldown;
    public double waypointX;
    public double waypointY;
    public double waypointZ;*/
    private Entity targetedEntity;

	public EntityFairyMob()
	{
		this(null);
	}

	public EntityFairyMob(World world)
	{
		super(world);
    	//this.setAlwaysRenderNameTag(true);
		this.setHealth(10F);
		this.setSize(0.5f, 0.5f);
        this.isImmuneToFire = true;
	}

    public int getMaxSpawnedInChunk()
    {
        return 1;
    }
    
    public void onLivingUpdate() {
    	super.onLivingUpdate();
    	this.onGround = true;
    	this.fallDistance = 0;
    	if (this.targetedEntity != null && this.isDead) {
    		this.targetedEntity = null;
    	}
        if (this.targetedEntity == null)
        {
            this.targetedEntity = this.worldObj.getClosestPlayerToEntity(this, 100.0D);
        }
        if (this.targetedEntity != null) {
        	this.faceEntity(targetedEntity, rotationYaw, rotationPitch);
        	float dist = (float)Math.sqrt(Math.pow(this.posX - targetedEntity.posX, 2) + Math.pow(this.posZ - targetedEntity.posZ, 2));
        	float vDist = (float)(targetedEntity.posY - this.posY);
        	this.motionX = this.posX + 1 < targetedEntity.posX ? dist/32 : this.posX - 1 > targetedEntity.posX ? -dist/32 : 0.0F;
        	this.motionY = this.posY + 1 < targetedEntity.posY ? vDist/32 : this.posY + 1 > targetedEntity.posY ? vDist/32 : 0.0F;
        	this.motionZ = this.posZ + 1 < targetedEntity.posZ ? dist/32 : this.posZ - 1 > targetedEntity.posZ ? -dist/32 : 0.0F;
        }
    }
    
    /*protected void updateEntityActionState()
    {
        this.despawnEntity();
        double d0 = this.waypointX - this.posX;
        double d1 = this.waypointY - this.posY;
        double d2 = this.waypointZ - this.posZ;
        double d3 = d0 * d0 + d1 * d1 + d2 * d2;

        if (d3 < 1.0D || d3 > 1600.0D)
        {
            this.waypointX = this.posX + (double)((this.rand.nextFloat() * 2.0F - 1.0F) * 16.0F);
            this.waypointY = this.posY + (double)((this.rand.nextFloat() * 2.0F - 1.0F) * 16.0F);
            this.waypointZ = this.posZ + (double)((this.rand.nextFloat() * 2.0F - 1.0F) * 16.0F);
        }

        if (this.courseChangeCooldown-- <= 0)
        {
            this.courseChangeCooldown += this.rand.nextInt(5) + 4;
            d3 = (double)MathHelper.sqrt_double(d3);

            if (this.isCourseTraversable(this.waypointX, this.waypointY, this.waypointZ, d3))
            {
                this.motionX += d0 / d3 * 0.05D;
                this.motionY += d1 / d3 * 0.05D;
                this.motionZ += d2 / d3 * 0.05D;
            }
            else
            {
                this.waypointX = this.posX;
                this.waypointY = this.posY;
                this.waypointZ = this.posZ;
            }
        }

        if (this.targetedEntity != null && this.targetedEntity.isDead)
        {
            this.targetedEntity = null;
        }

        if (this.targetedEntity == null)
        {
            this.targetedEntity = this.worldObj.getClosestPlayerToEntity(this, 100.0D);
        }

        double d4 = 64.0D;

        if (this.targetedEntity != null && this.targetedEntity.getDistanceSqToEntity(this) < d4 * d4)
        {
            double d5 = this.targetedEntity.posX - this.posX;
            double d6 = this.targetedEntity.boundingBox.minY + (double)(this.targetedEntity.height / 2.0F) - (this.posY + (double)(this.height / 2.0F));
            double d7 = this.targetedEntity.posZ - this.posZ;
            this.renderYawOffset = this.rotationYaw = -((float)Math.atan2(d5, d7)) * 180.0F / (float)Math.PI;
        }
        else
        {
            this.renderYawOffset = this.rotationYaw = -((float)Math.atan2(this.motionX, this.motionZ)) * 180.0F / (float)Math.PI;
        }
    }*/

    /*private boolean isCourseTraversable(double par1, double par3, double par5, double par7)
    {
        double d4 = (this.waypointX - this.posX) / par7;
        double d5 = (this.waypointY - this.posY) / par7;
        double d6 = (this.waypointZ - this.posZ) / par7;
        AxisAlignedBB axisalignedbb = this.boundingBox.copy();

        for (int i = 1; (double)i < par7; ++i)
        {
            axisalignedbb.offset(d4, d5, d6);

            if (!this.worldObj.getCollidingBoundingBoxes(this, axisalignedbb).isEmpty())
            {
                return false;
            }
        }

        return true;
    }*/
    
	/*private boolean oreInRange(float f) 
	{
		Block[] validOreBlocks = new Block[] {
			Block.oreCoal,
			Block.oreIron,
			Block.oreGold,
			Block.oreRedstone,
			Block.oreRedstoneGlowing,
			Block.oreLapis,
			Block.oreEmerald,
			Block.oreDiamond,
			Block.oreNetherQuartz,
			HuornForest.blockObsidianOre,
			HuornForest.blockAmethystOre
		};
		Minecraft mc = Minecraft.getMinecraft();
    	EntityPlayer player = mc.thePlayer;
    	if(player != null) {
            int i = MathHelper.floor_double(player.posX);
            int j = MathHelper.floor_double(player.posY);
            int k = MathHelper.floor_double(player.posZ);
            double radius = f;
            double radiusSq = radius * radius;
            int ceilRadius = (int)Math.ceil(radius);
            for (int x = 0; x <= ceilRadius; x++) {
                for (int y = 0; y <= ceilRadius; y++) {
                    for (int z = 0; z <= ceilRadius; z++) {
                        double dSq = (x * x + y * y + z * z);
                        if (dSq > radiusSq) { continue; }
                        for (int n = 0; n < validOreBlocks.length; n++) {
	                        if (this.worldObj.getBlockId(i + x, j + y, k + z) == validOreBlocks[n].blockID) {
	                        	return true;
	                        }
	                        if (this.worldObj.getBlockId(i - x, j + y, k + z) == validOreBlocks[n].blockID) {
	                        	return true;
	                        }
	                        if (this.worldObj.getBlockId(i + x, j - y, k + z) == validOreBlocks[n].blockID) {
	                        	return true;
	                        }
	                        if (this.worldObj.getBlockId(i + x, j + y, k - z) == validOreBlocks[n].blockID) {
	                        	return true;
	                        }
	                        if (this.worldObj.getBlockId(i - x, j - y, k + z) == validOreBlocks[n].blockID) {
	                        	return true;
	                        }
	                        if (this.worldObj.getBlockId(i + x, j - y, k - z) == validOreBlocks[n].blockID) {
	                        	return true;
	                        }
	                        if (this.worldObj.getBlockId(i - x, j + y, k - z) == validOreBlocks[n].blockID) {
	                        	return true;
	                        }
	                        if (this.worldObj.getBlockId(i - x, j - y, k - z) == validOreBlocks[n].blockID) {
	                        	return true;
	                        }
                        }
                    }
                }
            }
    	}
		return false;
	}*/
}

