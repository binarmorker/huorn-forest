package huornforest.entities;

import huornforest.common.HuornForest;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.util.ResourceLocation;

public class RenderFairyMob extends RenderLiving {

	public RenderFairyMob(ModelBase par1ModelBase, float par2) {
		super(par1ModelBase, par2);
		model = ((RenderFairyModel)mainModel);
	}

	protected RenderFairyModel model;
	private static final ResourceLocation textureLocation = new ResourceLocation("huornforest","textures/mobs/FairyTexture.png");
	
	@Override
	public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9) {
		super.doRenderLiving((EntityFairyMob)par1Entity, par2, par4, par6, par8, par9);
	}

	@Override
	public void doRenderLiving(EntityLiving par1EntityLiving, double par2, double par4, double par6, float par8, float par9) {
		super.doRenderLiving((EntityFairyMob)par1EntityLiving, par2, par4, par6, par8, par9);
	}
	
	@Override
	protected ResourceLocation getEntityTexture(Entity entity) {
		return textureLocation;
	}
	
    protected ResourceLocation getEntityTexture(EntityFairyMob par1EntityFairyMob)
    {
        return textureLocation;
    }
}
