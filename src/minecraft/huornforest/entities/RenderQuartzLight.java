package huornforest.entities;

import huornforest.blocks.BlockQuartzLight;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;

public class RenderQuartzLight extends RenderBlocks {

    public boolean renderBlockQuartzLight(BlockQuartzLight par1BlockQuartzLight, int par2, int par3, int par4)
    {
        float f = 0.1875F;
        this.setOverrideBlockTexture(this.getBlockIcon(Block.glass));
        this.setRenderBounds(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
        this.renderStandardBlock(par1BlockQuartzLight, par2, par3, par4);
        this.renderAllFaces = true;
        this.setOverrideBlockTexture(this.getBlockIcon(Block.obsidian));
        this.setRenderBounds(0.125D, 0.0062500000931322575D, 0.125D, 0.875D, (double)f, 0.875D);
        this.renderStandardBlock(par1BlockQuartzLight, par2, par3, par4);
        this.setOverrideBlockTexture(this.getBlockIcon(Block.beacon));
        this.setRenderBounds(0.1875D, (double)f, 0.1875D, 0.8125D, 0.875D, 0.8125D);
        this.renderStandardBlock(par1BlockQuartzLight, par2, par3, par4);
        this.renderAllFaces = false;
        this.clearOverrideBlockTexture();
        return true;
    }

}
