package huornforest.entities;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;

public class RenderFairyModel extends ModelBase
{
    ModelRenderer Body;
    ModelRenderer LeftWing;
    ModelRenderer RightWing;
    ModelRenderer SmallLeftWing;
    ModelRenderer SmallRightWing;
  
  public RenderFairyModel()
  {
    textureWidth = 12;
    textureHeight = 10;
    
      Body = new ModelRenderer(this, 0, 0);
      //Body.addBox(-1.5F, -1.5F, -1F, 3, 3, 3);
      Body.addBox(-1F, -1F, -0.75F, 2, 2, 2);
      Body.setRotationPoint(0F, 16F, 0F);
      Body.setTextureSize(12, 10);
      Body.mirror = true;
      setRotation(Body, 0F, 0F, 0F);
      LeftWing = new ModelRenderer(this, 0, 6);
      LeftWing.addBox(0F, -1F, 0F, 4, 2, 0);
      LeftWing.setRotationPoint(0F, 15F, 1F);
      LeftWing.setTextureSize(12, 10);
      LeftWing.mirror = false;
      setRotation(LeftWing, 0.2617994F, -0.2617994F, -0.5235988F);
      RightWing = new ModelRenderer(this, 0, 4);
      RightWing.addBox(-4F, -1F, 0F, 4, 2, 0);
      RightWing.setRotationPoint(0F, 15F, 1F);
      RightWing.setTextureSize(12, 10);
      RightWing.mirror = false;
      setRotation(RightWing, 0.2617994F, 0.2617994F, 0.5235988F);
      SmallLeftWing = new ModelRenderer(this, 0, 8);
      SmallLeftWing.addBox(-3F, -2F, 0.75F, 3, 2, 0);
      SmallLeftWing.setRotationPoint(0F, 18F, 1F);
      SmallLeftWing.setTextureSize(12, 10);
      SmallLeftWing.mirror = false;
      setRotation(SmallLeftWing, 0.2617994F, 0.2617994F, -0.5235988F);
      SmallRightWing = new ModelRenderer(this, 6, 8);
      SmallRightWing.addBox(0F, -2F, 0.75F, 3, 2, 0);
      SmallRightWing.setRotationPoint(0F, 18F, 1F);
      SmallRightWing.setTextureSize(12, 10);
      SmallRightWing.mirror = false;
      setRotation(SmallRightWing, 0.2617994F, -0.2617994F, 0.5235988F);
  }
  
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
	  	setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        this.RightWing.rotateAngleZ = MathHelper.cos(f2 * 2.0F) * (float)Math.PI * 0.20F;
        this.LeftWing.rotateAngleZ = -this.RightWing.rotateAngleZ;
        this.SmallRightWing.rotateAngleZ = MathHelper.sin(f2 * 2.0F) * (float)Math.PI * 0.20F;
        this.SmallLeftWing.rotateAngleZ = -this.SmallRightWing.rotateAngleZ;
	  	Body.render(f5);
	  	LeftWing.render(f5);
	  	RightWing.render(f5);
	  	SmallLeftWing.render(f5);
	  	SmallRightWing.render(f5);
	}
  
	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}
  
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}
}