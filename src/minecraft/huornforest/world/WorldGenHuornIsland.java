package huornforest.world;

import huornforest.common.HuornForest;
import huornforest.common.Perlin;

import java.util.Random;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraft.util.MathHelper;
import net.minecraftforge.event.terraingen.TerrainGen;

public class WorldGenHuornIsland
{	
    private boolean isPermitted(World w, int x, int z, int r)
    {
        if (w.getBiomeGenForCoords(x, z) == BiomeGenBase.ocean ||
                w.getBiomeGenForCoords(x, z) == BiomeGenBase.river)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean generate(World par1World, Random par2Random, int par3, int par4, int par5, int par6)
    {
        if (par2Random.nextInt(250) == 0 && isPermitted(par1World, par3, par5, par6))
        {
            createIsland(par1World, par2Random, par3, par4, par5, par6);
            HuornForest.log("Island generated on " + par3 + ", " + par4 + ", " + par5);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void createIsland(World par1World, Random par2Random, int par3, int par4, int par5, int par6)
    {
        par6 *= 2;
        par3 -= par6 / 2;
        par5 -= par6 / 2;
        int templeRad = 8;
        int centerTop = 0;
        long seed = par1World.getSeed();
        Perlin octave0 = new Perlin(seed, 10);
        Perlin octave1 = new Perlin(seed / 2, 5);
        Perlin octave2 = new Perlin(seed ^ 2, 20);
        Perlin octave3 = new Perlin(seed / 3, 18);
        Perlin octave4 = new Perlin(seed ^ 3, 14);
        Perlin octave5 = new Perlin(seed * 2, 7);

        for (int x = 0; x < par6; x++)
        {
            for (int z = 0; z < par6; z++)
            {
                double noiseTop = 1 * octave0.getNoiseLevelAtPosition(par3 + x, par5 + z)
                                  + 2 * octave1.getNoiseLevelAtPosition(par3 + x, par5 + z);
                double noiseBottom = 10 * octave2.getNoiseLevelAtPosition(par3 + x, par5 + z)
                                     + 6 * octave3.getNoiseLevelAtPosition(par3 + x, par5 + z)
                                     + 4 * octave4.getNoiseLevelAtPosition(par3 + x, par5 + z)
                                     + 8 * octave5.getNoiseLevelAtPosition(par3 + x, par5 + z);
                noiseTop -= (par6 / 32) * 2D * Math.cos(z / (par6 / (2 * Math.PI)));
                noiseTop -= (par6 / 32) * 2D * Math.cos(x / (par6 / (2 * Math.PI)));
                noiseBottom += (par6 / 32) * 4D * Math.cos(z / (par6 / (2 * Math.PI)));
                noiseBottom += (par6 / 32) * 4D * Math.cos(x / (par6 / (2 * Math.PI)));
                double diff = noiseTop - noiseBottom;

                for (int i = 0; i <= diff; i++)
                {
                    if (i == 0)
                    {
                        par1World.setBlock(par3 + x, par4 + (int)noiseTop, par5 + z, HuornForest.blockHumus.blockID);
                    }
                    else if (i <= 2)
                    {
                        par1World.setBlock(par3 + x, par4 + (int)noiseTop - i, par5 + z, Block.dirt.blockID);
                    }
                    else
                    {
                        par1World.setBlock(par3 + x, par4 + (int)noiseTop - i, par5 + z, Block.stone.blockID);
                        if (par2Random.nextInt(30) == 0)
                        	par1World.setBlock(par3 + x, par4 + (int)noiseTop - i, par5 + z, Block.oreCoal.blockID);
                        if (par2Random.nextInt(50) == 1)
                        	par1World.setBlock(par3 + x, par4 + (int)noiseTop - i, par5 + z, Block.oreIron.blockID);
                        if (par2Random.nextInt(80) == 2)
                        	par1World.setBlock(par3 + x, par4 + (int)noiseTop - i, par5 + z, Block.oreGold.blockID);
                        if (par2Random.nextInt(110) == 3)
                        	par1World.setBlock(par3 + x, par4 + (int)noiseTop - i, par5 + z, Block.oreEmerald.blockID);
                        if (par2Random.nextInt(150) == 4)
                        	par1World.setBlock(par3 + x, par4 + (int)noiseTop - i, par5 + z, HuornForest.blockAmethystOre.blockID);
                    }
                }

                if (par2Random.nextInt(20) == 0 && (x < (par6 / 2) - templeRad || x > (par6 / 2) + templeRad || z < (par6 / 2) - templeRad || z > (par6 / 2) + templeRad))
                {
                    (new WorldGenHuornTree(true)).generate(par1World, par2Random, par3 + x, par4 + (int)noiseTop, par5 + z);
                }

                if (x == par6 / 2 && z == par6 / 2)
                {
                    centerTop = (int)noiseTop;
                }
            }
        }
        new WorldGenHuornTemple().generate(par1World, par2Random, par3 + par6 / 2, par4 + centerTop - 1, par5 + par6 / 2);
    }
}
