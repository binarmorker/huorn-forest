package huornforest.world;

import huornforest.common.HuornForest;

import java.util.Random;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;

public class WorldGenGlobal implements IWorldGenerator
{
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
    {
        switch (world.provider.dimensionId)
        {
                //case -1: generateEnd(world, random, chunkX*16, chunkZ*16);
            case 0:
                generateSurface(world, random, chunkX * 16, chunkZ * 16);
                //case 1: generateNether(world, random, chunkX*16, chunkZ*16);
        }
    }

    private void generateSurface(World world, Random random, int blockX, int blockZ)
    {
        int Xcoord = blockX + random.nextInt(16);
        int Ycoord = random.nextInt(30);
        int Zcoord = blockZ + random.nextInt(16);
        (new WorldGenMinable(HuornForest.blockObsidianOre.blockID, 1, 10)).generate(world, random, Xcoord, Ycoord, Zcoord);
        int Xcoord1 = blockX + random.nextInt(16);
        int Ycoord1 = 140;
        int Zcoord1 = blockZ + random.nextInt(16);
        int radius = 56;
        (new WorldGenHuornIsland()).generate(world, random, Xcoord1, Ycoord1, Zcoord1, radius);
    }

    /*private void generateNether(World world, Random random, int blockX, int blockZ)
    {
    	int Xcoord = blockX + random.nextInt(16);
    	int Ycoord = random.nextInt(60);
    	int Zcoord = blockZ + random.nextInt(16);
    	(new WorldGenMinableNether(Tutorial.oreblock.blockID, 1, 10)).generate(world, random, Xcoord, Ycoord, Zcoord);
    }*/
}