package huornforest.blocks.brewing;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class GuiBrewing extends GuiContainer
{
    private static final ResourceLocation field_110410_t = new ResourceLocation("huornforest","textures/gui/condensator.png");
    private TileEntityBrewing furnaceInventory;

    public GuiBrewing(InventoryPlayer par1InventoryPlayer, TileEntityBrewing par2TileEntityBrewing)
    {
        super(new ContainerBrewing(par1InventoryPlayer, par2TileEntityBrewing));
        this.furnaceInventory = par2TileEntityBrewing;
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items)
     */
    protected void drawGuiContainerForegroundLayer(int par1, int par2)
    {
        String s = this.furnaceInventory.isInvNameLocalized() ? this.furnaceInventory.getInvName() : I18n.getString(this.furnaceInventory.getInvName());
        this.fontRenderer.drawString(s, this.xSize / 2 - this.fontRenderer.getStringWidth(s) / 2, 6, 4210752);
        this.fontRenderer.drawString(I18n.getString("container.inventory"), 8, this.ySize - 96 + 2, 4210752);
    }

    /**
     * Draw the background layer for the GuiContainer (everything behind the items)
     */
    protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(field_110410_t);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
        int i1;
        if (this.furnaceInventory.isBurning())
        {
            i1 = this.furnaceInventory.getBurnTimeRemainingScaled(30);
            this.drawTexturedModalRect(k + 116, l + 50 + 28 - i1, 186, 28 - i1, 12, 30);
        }

        i1 = this.furnaceInventory.getCookProgressScaled(30);
        this.drawTexturedModalRect(k + 84, l + 51 + 28 - i1, 176, 28 - i1, 9, 30);
    }
}
