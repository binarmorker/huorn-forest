package huornforest.blocks.brewing;

import huornforest.common.HuornForest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class BrewingRecipes
{
    private static final BrewingRecipes smeltingBase = new BrewingRecipes();

    /** The list of smelting results. */
    private Map smeltingList = new HashMap();
    private Map experienceList = new HashMap();

    /**
     * Used to call methods addSmelting and getSmeltingResult.
     */
    public static final BrewingRecipes smelting()
    {
        return smeltingBase;
    }

    private BrewingRecipes()
    {
        this.addSmelting(new ItemStack(Item.potion, 1, 0), new ItemStack(Item.gunpowder), new ItemStack(HuornForest.itemHuornPotion, 1, 0), 1.0F);
        this.addSmelting(new ItemStack(HuornForest.itemHuornPotion, 1, 0), new ItemStack(Item.gunpowder), new ItemStack(HuornForest.itemHuornPotion, 1, 1), 2.0F);
        this.addSmelting(new ItemStack(HuornForest.itemHuornPotion, 1, 1), new ItemStack(Item.gunpowder), new ItemStack(HuornForest.itemHuornPotion, 1, 2), 3.0F);
        this.addSmelting(new ItemStack(Item.potion, 1, 16427), new ItemStack(Item.gunpowder), new ItemStack(HuornForest.itemHuornSplashPotion, 1, 0), 1.0F);
        this.addSmelting(new ItemStack(HuornForest.itemHuornSplashPotion, 1, 0), new ItemStack(Item.gunpowder), new ItemStack(HuornForest.itemHuornSplashPotion, 1, 1), 2.0F);
        this.addSmelting(new ItemStack(HuornForest.itemHuornSplashPotion, 1, 1), new ItemStack(Item.gunpowder), new ItemStack(HuornForest.itemHuornSplashPotion, 1, 2), 3.0F);
    }

    /**
     * Adds a smelting recipe.
     */
    public void addSmelting(ItemStack par1ItemStack, ItemStack par2ItemStack, ItemStack par3ItemStack, float par4)
    {
    	int item1 = (par1ItemStack.itemID * 6) + (par1ItemStack.getItemDamage() * 9);
    	int item2 = (par2ItemStack.itemID * 4) + (par2ItemStack.getItemDamage() * 2);
    	int item3 = par3ItemStack.getItemDamage();
        this.smeltingList.put(item1 + item2, item3);
        this.experienceList.put(item3, par4);
    }

    public Map getSmeltingList()
    {
        return this.smeltingList;
    }

    /**
     * Used to get the resulting ItemStack form a source ItemStack
     * @param item The Source ItemStack
     * @return The result ItemStack
     */
    public ItemStack getSmeltingResult(ItemStack par1ItemStack, ItemStack par2ItemStack) 
    {
        if (par1ItemStack == null || par2ItemStack == null)
        {
            return null;
        }
    	int item1 = (par1ItemStack.itemID * 5) + (par1ItemStack.getItemDamage() * 2);
    	int item2 = (par2ItemStack.itemID * 4) + (par2ItemStack.getItemDamage() * 3);
    	int item3;
        Object ret = smeltingList.get(item1 + item2);
        if (ret != null) 
        {
        	if (par1ItemStack.itemID == Item.potion.itemID) {
        		if (par1ItemStack.getItemDamage() == 16427) {
        			item3 = HuornForest.itemHuornSplashPotion.itemID;
        		} else {
        			item3 = HuornForest.itemHuornPotion.itemID;
        		}
        	} else {
        		item3 = par1ItemStack.itemID;
        	}
            return new ItemStack(item3, 0, (Integer)(ret));
        }
        return null;
    }

    /**
     * Grabs the amount of base experience for this item to give when pulled from the furnace slot.
     */
    public float getExperience(ItemStack item)
    {
        /*if (item == null || item.getItem() == null)
        {
            return 0;
        }
        float ret = item.getItem().getSmeltingExperience(item);
        if (ret < 0 && metaExperience.containsKey(Arrays.asList(item.itemID, item.getItemDamage())))
        {
            ret = metaExperience.get(Arrays.asList(item.itemID, item.getItemDamage()));
        }
        if (ret < 0 && experienceList.containsKey(item.itemID))
        {
            ret = ((Float)experienceList.get(item.itemID)).floatValue();
        }
        return (ret < 0 ? 0 : ret);*/
    	/**
    	 * Temporarily sets this as a constant.
    	 */
    	return 1.0F;
    }
}
