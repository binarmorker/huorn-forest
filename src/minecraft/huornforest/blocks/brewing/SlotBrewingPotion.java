package huornforest.blocks.brewing;

import huornforest.common.HuornForest;
import huornforest.items.ItemHuornPotion;
import huornforest.items.ItemSplashHuornPotion;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.AchievementList;

class SlotBrewingPotion extends Slot
{
    /** The player that has this container open. */
    private EntityPlayer player;

    public SlotBrewingPotion(EntityPlayer par1EntityPlayer, IInventory par2IInventory, int par3, int par4, int par5)
    {
        super(par2IInventory, par3, par4, par5);
        this.player = par1EntityPlayer;
    }

    /**
     * Check if the stack is a valid item for this slot. Always true beside for the armor slots.
     */
    public boolean isItemValid(ItemStack par1ItemStack)
    {
        return canHoldPotion(par1ItemStack);
    }

    /**
     * Returns the maximum stack size for a given slot (usually the same as getInventoryStackLimit(), but 1 in the case
     * of armor slots)
     */
    public int getSlotStackLimit()
    {
        return 1;
    }

    public void onPickupFromSlot(EntityPlayer par1EntityPlayer, ItemStack par2ItemStack)
    {
        if (par2ItemStack.getItem() instanceof ItemHuornPotion 
        		|| par2ItemStack.getItem() instanceof ItemSplashHuornPotion)
        {
            this.player.addStat(AchievementList.potion, 1);
        }

        super.onPickupFromSlot(par1EntityPlayer, par2ItemStack);
    }

    /**
     * Returns true if this itemstack can be filled with a potion
     */
    public static boolean canHoldPotion(ItemStack par0ItemStack)
    {
        return par0ItemStack != null && (par0ItemStack.getItem() instanceof ItemHuornPotion 
        		|| par0ItemStack.getItem() instanceof ItemSplashHuornPotion 
        		|| (par0ItemStack.itemID == Item.potion.itemID 
        		&& (par0ItemStack.getItemDamage() == 0 
        		|| par0ItemStack.getItemDamage() == 16427)));
    }
}
