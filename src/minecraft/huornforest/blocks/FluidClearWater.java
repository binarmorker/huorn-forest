package huornforest.blocks;

import huornforest.common.HuornForest;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.Icon;
import net.minecraftforge.fluids.Fluid;

public class FluidClearWater extends Fluid
{
  public FluidClearWater(String fluidName)
  {
    super(fluidName);
  }

  @SideOnly(Side.CLIENT)
  public Icon getStillIcon()
  {
    return BlockClearWater.clearWaterStillIcon;
  }

  @SideOnly(Side.CLIENT)
  public Icon getFlowingIcon()
  {
    return BlockClearWater.clearWaterFlowingIcon;
  }
}