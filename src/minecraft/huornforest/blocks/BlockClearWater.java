package huornforest.blocks;

import huornforest.common.HuornForest;
import huornforest.entities.EntityHuornSteamFX;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFlowing;
import net.minecraft.block.material.Material;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntitySmokeFX;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.Teleporter;
import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.BlockFluidClassic;

public class BlockClearWater extends BlockFluidClassic
{
	public static Icon clearWaterStillIcon;
	public static Icon clearWaterFlowingIcon;
	  
    public BlockClearWater(int par1, Fluid par2Fluid, Material par3Material)
    {
        super(par1, par2Fluid, par3Material);
        this.stack = new FluidStack(par2Fluid, 1000);
        for (int i = 8; i < 11; i++)
        {
        	this.displacementIds.put(Integer.valueOf(i), Boolean.valueOf(false));
        }

        this.displacementIds.put(Integer.valueOf(this.blockID), Boolean.valueOf(false));
    }

    @Override
    public boolean canCreatureSpawn(EnumCreatureType type, World world, int x, int y, int z) {
    	return false;
    }
    
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
    	clearWaterStillIcon = par1IconRegister.registerIcon("huornforest:clearWater_still");
    	clearWaterFlowingIcon = par1IconRegister.registerIcon("huornforest:clearWater_flow");
    }

    @SideOnly(Side.CLIENT)
    public Icon getIcon(int par1, int par2)
    {
    	return (par1 != 0) && (par1 != 1) ? clearWaterFlowingIcon : clearWaterStillIcon;
    }
    
    @Override
    public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random) {
    	super.randomDisplayTick(par1World, par2, par3, par4, par5Random);
		for (int n=0; n<3; n++)
	    	if (par1World.isAirBlock(par2, par3+1, par4) && par5Random.nextInt(1) == 0)
	        {
	    		Minecraft.getMinecraft().effectRenderer.addEffect(new EntityHuornSteamFX(par1World, par2+par5Random.nextFloat(), par3+1.0D, par4+par5Random.nextFloat(), 0.0D, 0.0D, 0.0D));
	        }
    }
    
    public boolean canDisplace(IBlockAccess world, int x, int y, int z)
    {
    	int bId = world.getBlockId(x, y, z);

    	if (bId == 0)
    		return true;
    	if (bId == this.blockID)
    		return false;
    	if (this.displacementIds.containsKey(Integer.valueOf(bId)))
    		return ((Boolean)this.displacementIds.get(Integer.valueOf(bId))).booleanValue();
    	Material material = Block.blocksList[bId].blockMaterial;

    	if (material.blocksMovement() || material == Material.water || material == Material.lava || material == Material.portal)
    		return false;
    	return true;
    }

    public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity)
    {
    }
}
