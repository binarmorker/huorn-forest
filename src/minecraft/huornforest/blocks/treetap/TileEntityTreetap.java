package huornforest.blocks.treetap;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;

public class TileEntityTreetap extends TileEntity {

	private static final String NBT_CLICK_AMOUNT = "ClickedAmount";
	private int clicked = 0;
	
	public TileEntityTreetap() { }
	
	public int getClickAmount() {
		return clicked;
	}
	
	public void setClickAmount(int newAmount) {
		clicked = newAmount;
	}
	
	public void addClick() {
		clicked++;
	}
	
	@Override
	public void updateEntity() {
		if (clicked > 0) {
			this.worldObj.spawnParticle("smoke", xCoord, yCoord, zCoord, 0.0D, 0.1D, 0.0D);
		}
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		clicked = nbt.getInteger(NBT_CLICK_AMOUNT);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setInteger(NBT_CLICK_AMOUNT, clicked);
	}
	
	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound tileTag = new NBTTagCompound();
		this.writeToNBT(tileTag);
		return new Packet132TileEntityData(this.xCoord, this.yCoord, this.zCoord, 0, tileTag);
	}
	
	@Override
	public void onDataPacket(INetworkManager net, Packet132TileEntityData pkt) {
		this.readFromNBT(pkt.data);
	}
	
}
