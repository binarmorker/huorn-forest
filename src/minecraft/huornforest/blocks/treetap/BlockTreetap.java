package huornforest.blocks.treetap;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import huornforest.common.HuornForest;
import huornforest.entities.EntityGrenade;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class BlockTreetap extends BlockContainer
{
    public BlockTreetap(int par1)
    {
        super(par1, Material.wood);
    }
    
    public int quantityDropped(Random par1Random)
    {
        return 0;
    }

    public int idDropped(int par1, Random par2Random, int par3)
    {
        return 0;
    }

    @Override
    public void onBlockClicked(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer) {
    	TileEntityTreetap te = (TileEntityTreetap)par1World.getBlockTileEntity(par2, par3, par4);
    	if (te != null) {
    		te.addClick();
    		HuornForest.debug(te.getClickAmount());
		} else {
    		HuornForest.debug("Something went wrong.");
		}
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.blockIcon = par1IconRegister.registerIcon("huornforest:" + this.getUnlocalizedName().replace("tile.", ""));
    }

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityTreetap();
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, int i, int j) {
		world.removeBlockTileEntity(x, y, z);
	}
}
