package huornforest.blocks;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockBlueQuartz extends Block
{
    @SideOnly(Side.CLIENT)
    private Icon side;
    @SideOnly(Side.CLIENT)
    private Icon top;

    public BlockBlueQuartz(int par1)
    {
        super(par1, Material.rock);
    }

    public int getRenderType()
    {
        return 31;
    }

    public int quantityDropped(Random par1Random)
    {
        return 1;
    }

    public int idDropped(int par1, Random par2Random, int par3)
    {
        return this.blockID;
    }

    @Override
    public boolean canCreatureSpawn(EnumCreatureType type, World world, int x, int y, int z) {
    	return false;
    }
    
    @SideOnly(Side.CLIENT)
    public Icon getIcon(int par1, int par2)
    {
        int k = par2 & 12;
        int l = par2 & 3;
        return k == 0 && (par1 == 1 || par1 == 0) ? this.top : (k == 4 && (par1 == 5 || par1 == 4) ? this.top : (k == 8 && (par1 == 2 || par1 == 3) ? this.top : this.side));
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.top = par1IconRegister.registerIcon("huornforest:" + this.getUnlocalizedName().replace("tile.", "") + "Top");
        this.side = par1IconRegister.registerIcon("huornforest:" + this.getUnlocalizedName().replace("tile.", ""));
    }
}
