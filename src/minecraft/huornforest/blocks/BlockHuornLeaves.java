package huornforest.blocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import huornforest.common.HuornForest;
import net.minecraft.block.Block;
import net.minecraft.block.BlockGrass;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockLeavesBase;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.Icon;
import net.minecraft.world.ColorizerFoliage;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.IShearable;

public class BlockHuornLeaves extends BlockLeavesBase
{
    int[] adjacentTreeBlocks;
    public boolean graphicsLevel;
    @SideOnly(Side.CLIENT)
    Icon blockIconFancy;
    @SideOnly(Side.CLIENT)
    Icon blockIconOpaque;

    public BlockHuornLeaves(int par1)
    {
        super(par1, Material.leaves, false);
        setBurnProperties(this.blockID, 50, 10);
        this.setStepSound(soundGrassFootstep);
        this.setTickRandomly(true);
    }

	public int getBlockColor()
	{
		return 0;
	}

    @Override
    public boolean canCreatureSpawn(EnumCreatureType type, World world, int x, int y, int z) {
    	return false;
    }
    
	public void onBlockRemoval(World world, int i, int j, int k)
	{
		int l = 1;
		int i1 = l + 1;
		if(world.checkChunksExist(i - i1, j - i1, k - i1, i + i1, j + i1, k + i1))
		{
			for(int j1 = -l; j1 <= l; j1++)
			{
				for(int k1 = -l; k1 <= l; k1++)
				{
					for(int l1 = -l; l1 <= l; l1++)
					{
						int i2 = world.getBlockId(i + j1, j + k1, k + l1);
						if(i2 == HuornForest.blockHuornLeaves.blockID)
						{
							int j2 = world.getBlockMetadata(i + j1, j + k1, k + l1);
							world.setBlock(i + j1, j + k1, k + l1, j2 | 8);
						}
					}
				}
			}
		}
	}

	public void updateTick(World world, int i, int j, int k, Random random)
	{
		if(world.isRemote)
			return;
		int l = world.getBlockMetadata(i, j, k);
		if((l & 8) != 0 && (l & 4) == 0)
		{
			byte byte0 = 4;
			int i1 = byte0 + 1;
			byte byte1 = 32;
			int j1 = byte1 * byte1;
			int k1 = byte1 / 2;
			if(adjacentTreeBlocks == null)
				adjacentTreeBlocks = new int[byte1 * byte1 * byte1];
			if(world.checkChunksExist(i - i1, j - i1, k - i1, i + i1, j + i1, k + i1))
			{
				for(int l1 = -byte0; l1 <= byte0; l1++)
				{
					for(int k2 = -byte0; k2 <= byte0; k2++)
					{
						for(int i3 = -byte0; i3 <= byte0; i3++)
						{
							int k3 = world.getBlockId(i + l1, j + k2, k + i3);
							if(k3 == HuornForest.blockHuornLeaves.blockID)
								adjacentTreeBlocks[(l1 + k1) * j1 + (k2 + k1) * byte1 + (i3 + k1)] = 0;
							else
								if(k3 == HuornForest.blockHuornLeaves.blockID)
									adjacentTreeBlocks[(l1 + k1) * j1 + (k2 + k1) * byte1 + (i3 + k1)] = -2;
								else
									adjacentTreeBlocks[(l1 + k1) * j1 + (k2 + k1) * byte1 + (i3 + k1)] = -1;
						}
					}
				}
				for(int i2 = 1; i2 <= 4; i2++)
				{
					for(int l2 = -byte0; l2 <= byte0; l2++)
					{
						for(int j3 = -byte0; j3 <= byte0; j3++)
						{
							for(int l3 = -byte0; l3 <= byte0; l3++)
								if(adjacentTreeBlocks[(l2 + k1) * j1 + (j3 + k1) * byte1 + (l3 + k1)] == i2 - 1)
								{
									if(adjacentTreeBlocks[((l2 + k1) - 1) * j1 + (j3 + k1) * byte1 + (l3 + k1)] == -2)
										adjacentTreeBlocks[((l2 + k1) - 1) * j1 + (j3 + k1) * byte1 + (l3 + k1)] = i2;
									if(adjacentTreeBlocks[(l2 + k1 + 1) * j1 + (j3 + k1) * byte1 + (l3 + k1)] == -2)
										adjacentTreeBlocks[(l2 + k1 + 1) * j1 + (j3 + k1) * byte1 + (l3 + k1)] = i2;
									if(adjacentTreeBlocks[(l2 + k1) * j1 + ((j3 + k1) - 1) * byte1 + (l3 + k1)] == -2)
										adjacentTreeBlocks[(l2 + k1) * j1 + ((j3 + k1) - 1) * byte1 + (l3 + k1)] = i2;
									if(adjacentTreeBlocks[(l2 + k1) * j1 + (j3 + k1 + 1) * byte1 + (l3 + k1)] == -2)
										adjacentTreeBlocks[(l2 + k1) * j1 + (j3 + k1 + 1) * byte1 + (l3 + k1)] = i2;
									if(adjacentTreeBlocks[(l2 + k1) * j1 + (j3 + k1) * byte1 + ((l3 + k1) - 1)] == -2)
										adjacentTreeBlocks[(l2 + k1) * j1 + (j3 + k1) * byte1 + ((l3 + k1) - 1)] = i2;
									if(adjacentTreeBlocks[(l2 + k1) * j1 + (j3 + k1) * byte1 + (l3 + k1 + 1)] == -2)
										adjacentTreeBlocks[(l2 + k1) * j1 + (j3 + k1) * byte1 + (l3 + k1 + 1)] = i2;
								}
						}
					}
				}
			}
			int j2 = adjacentTreeBlocks[k1 * j1 + k1 * byte1 + k1];
			if(j2 >= 0)
				world.setBlock(i, j, k, this.blockID, l & -9, 2);
			else
				removeLeaves(world, i, j, k);
		}
        if (random.nextInt(150) == 0 && world.isAirBlock(i, j - 1, k))
        {
            this.dropBlockAsItem(world, i, j, k, world.getBlockMetadata(i, j, k), 0);
        }
	}

	private void removeLeaves(World world, int i, int j, int k)
	{
		dropBlockAsItem(world, i, j, k, world.getBlockMetadata(i, j, k), 0);
		world.setBlock(i, j, k, 0, 0, 3);
	}

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister reg)
    {
        this.blockIconFancy = reg.registerIcon("huornforest:" + this.getUnlocalizedName().replace("tile.", ""));
        this.blockIconOpaque = reg.registerIcon("huornforest:" + this.getUnlocalizedName().replace("tile.", "") + "Opaque");
        this.blockIcon = this.graphicsLevel ? this.blockIconFancy : this.blockIconOpaque;
    }

    @SideOnly(Side.CLIENT)
    public void setGraphicsLevel(boolean par1)
    {
        this.graphicsLevel = par1;
        this.blockIcon = this.graphicsLevel ? this.blockIconFancy : this.blockIconOpaque;
    }

	public boolean isOpaqueCube()
	{
		return !graphicsLevel;
	}

    @SideOnly(Side.CLIENT)
    public Icon getIcon(int par1, int par2)
    {
        return this.blockIcon;
    }

	public void dropBlockAsItemWithChance(World world, int i, int j, int k, int l, float f, int i1)
	{
		super.dropBlockAsItemWithChance(world, i, j, k, l, f, i1);
		if(!world.isRemote && (l & 3) == 0 && world.rand.nextInt(200) == 0)
			dropBlockAsItem_do(world, i, j, k, new ItemStack(HuornForest.itemHuornLeaf, 1, 0));
	}

	public void harvestBlock(World world, EntityPlayer entityplayer, int i, int j, int k, int l)
	{
		if(!world.isRemote && entityplayer.getCurrentEquippedItem() != null && entityplayer.getCurrentEquippedItem().itemID == Item.shears.itemID) {
			entityplayer.addStat(StatList.mineBlockStatArray[blockID], 1);
			dropBlockAsItem_do(world, i, j, k, new ItemStack(Block.leaves.blockID, 1, l & 3));
		} else {
			super.harvestBlock(world, entityplayer, i, j, k, l);
		}
	}

    public int damageDropped(int par1)
    {
        return 0;
    }

	public void onEntityWalking(World world, int i, int j, int k, Entity entity)
	{
		super.onEntityWalking(world, i, j, k, entity);
	}

    public int idDropped(int par1, Random par2Random, int par3)
    {
        return HuornForest.itemHuornLeaf.itemID;
    }

    public int quantityDropped(Random par1Random)
    {
        int rnd = par1Random.nextInt(150);

        if (rnd == 0)
        {
            return 2;
        }
        else if ((rnd % 50) == 5)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5)
    {
    	if (this.graphicsLevel)
    		return true;
    	else 
    		return super.shouldSideBeRendered(par1IBlockAccess, par2, par3, par4, par5);
    }
}