package huornforest.blocks;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import huornforest.common.HuornForest;
import huornforest.entities.EntityGrenade;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class BlockQuartzLight extends Block
{
    public BlockQuartzLight(int par1)
    {
        super(par1, Material.glass);
    }

    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock()
    {
        return false;
    }

 // getRenderType()
 // ------------------------
 // -1: Sign
 // 0 : Block
 // 1 : Reed
 // 2 : Torch
 // 3 : Fire
 // 4 : Fluid
 // 5 : Redstone Wire
 // 6 : Crops
 // 7 : Door
 // 8 : Ladder
 // 9 : Rail
 // 10: Stairs
 // 11: Fence
 // 12: Lever
 // 13: Cactus
 // 14: Bed
 // 15: Redstone Repeater
 // 16: Piston Base
 // 17: Piston Extension
 // 18: Pane
 // 19: Stems
 // 20: Vines
 // 21: Fence Gate
 // 22: Chest
 // 23: Lilypad
 // 24: Cauldron
 // 25: Brewing Stand
 // 26: End Portal Frame
 // 27: Dragon Egg
 // 28: Cocoa
 // 29: Trip Wire Source
 // 30: Trip Wire
 // 31: Log
 // 32: Wall
 // 33: Flower Pot
 // 34: Beacon
 // 35: Anvil 
 // 36: Redstone Logic
 // 37: Comparator
 // 38: Hopper
 // 39: Quartz

    @Override
    public int getRenderType()
    {
        return 0;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.blockIcon = par1IconRegister.registerIcon("huornforest:" + this.getUnlocalizedName().replace("tile.", ""));
    }

}
