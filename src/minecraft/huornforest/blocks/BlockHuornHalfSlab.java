package huornforest.blocks;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import huornforest.common.HuornForest;
import huornforest.entities.EntityGrenade;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHalfSlab;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class BlockHuornHalfSlab extends BlockHalfSlab
{
    public BlockHuornHalfSlab(int par1, boolean par2)
    {
        super(par1, par2, Material.wood);
        setBurnProperties(this.blockID, 50, 10);
        useNeighborBrightness[this.blockID] = true;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.blockIcon = par1IconRegister.registerIcon("huornforest:" + HuornForest.blockHuornWood.getUnlocalizedName().replace("tile.", ""));
    }

    @Override
    public boolean canCreatureSpawn(EnumCreatureType type, World world, int x, int y, int z) {
    	return false;
    }
    
    @Override
    public String getFullSlabName(int i)
    { 
        return this.getUnlocalizedName();
    }
}