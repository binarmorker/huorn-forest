package huornforest.items;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class ItemObsidian extends ItemMultiCommon
{
    public static final String[] item_a = new String[]
    {
        "normal",
        "small"
    };
    @SideOnly(Side.CLIENT)
    private Icon[] item_b;
    
    public ItemObsidian(int par1)
    {
        super(par1);
    }

    @SideOnly(Side.CLIENT)
    public Icon getIconFromDamage(int par1)
    {
        int j = MathHelper.clamp_int(par1, 0, item_a.length - 1);
        return item_b[j];
    }

    @SideOnly(Side.CLIENT)
    public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List)
    {
        for (int j = 0; j < item_a.length; ++j)
        {
            par3List.add(new ItemStack(par1, 1, j));
        }
    }

    public String getUnlocalizedName(ItemStack par1ItemStack)
    {
        int i = MathHelper.clamp_int(par1ItemStack.getItemDamage(), 0, item_a.length - 1);
        return super.getUnlocalizedName() + "_" + item_a[i];
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.item_b = new Icon[item_a.length];

        for (int i = 0; i < item_a.length; ++i)
        {
            this.item_b[i] = par1IconRegister.registerIcon("huornforest:" + this.getUnlocalizedName().replace("item.", "") + "_" + item_a[i]);
        }
    }
}
