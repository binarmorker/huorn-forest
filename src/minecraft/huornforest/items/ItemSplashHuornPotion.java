package huornforest.items;

import java.util.Iterator;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import huornforest.common.HuornForest;
import huornforest.entities.EntityGrenade;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntitySnowball;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSnowball;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionHelper;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class ItemSplashHuornPotion extends ItemSnowball
{
    public static final String[] itemMetaNames = new String[]
    {
        "grenade",
        "grenade1",
        "grenade2",
        "napalm",
        "napalm1",
        "napalm2",
        "miners",
        "miners1",
        "miners2",
        "ender",
        "ender1",
        "ender2"
    };
    public static final String[] item_a = new String[]
    {
        "splashGrenade",
        "splashNapalm",
        "splashMiners",
        "splashEnder",
        "entitySplashPotion"
    };
    @SideOnly(Side.CLIENT)
    private Icon[] item_b;

    public ItemSplashHuornPotion(int id)
    {
        super(id);
        this.setHasSubtypes(true);
        this.setMaxStackSize(1);
        this.setMaxDamage(0);
    }

    @SideOnly(Side.CLIENT)
    public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List)
    {
        for (int j = 0; j < itemMetaNames.length; ++j)
        {
            par3List.add(new ItemStack(par1, 1, j));
        }
    }

    @SideOnly(Side.CLIENT)
    public Icon getIconFromDamage(int par1)
    {
        int j = MathHelper.clamp_int(par1, 0, itemMetaNames.length);
        return item_b[j/3];
    }

    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {
    	String def = this.getUnlocalizedName() + "." + itemMetaNames[par1ItemStack.getItemDamage()] + ".effect";
        if (par1ItemStack.getItemDamage() / 3 == 0 || par1ItemStack.getItemDamage() / 3 == 1)
        	par3List.add(EnumChatFormatting.RED + "" + StatCollector.translateToLocal(def));
        else
        	par3List.add("" + StatCollector.translateToLocal(def));
    }

    public String getUnlocalizedName(ItemStack par1ItemStack)
    {
        int i = MathHelper.clamp_int(par1ItemStack.getItemDamage(), 0, itemMetaNames.length - 1);
        return super.getUnlocalizedName() + "." + item_a[i/3];
    }

    @Override
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.item_b = new Icon[item_a.length];

        for (int i = 0; i < item_a.length; ++i)
        {
            this.item_b[i] = par1IconRegister.registerIcon("huornforest:" + item_a[i]);
        }
    }

    @Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (!par3EntityPlayer.capabilities.isCreativeMode)
        {
            --par1ItemStack.stackSize;
        }

        par2World.playSoundAtEntity(par3EntityPlayer, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));
        
        EntityGrenade grenade = new EntityGrenade(par2World, par3EntityPlayer, par1ItemStack.getItemDamage(), 40);

        if (!par2World.isRemote)
        {
            par2World.spawnEntityInWorld(grenade);
        }
        
        return par1ItemStack;
    }
}
