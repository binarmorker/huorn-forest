package huornforest.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemReed;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemPlacable extends ItemReed
{
    ItemStack itemleft = null;

    public ItemPlacable(int par1, Block par2Block)
    {
        super(par1, par2Block);
        this.setMaxDamage(0);
    }

    public ItemPlacable(int par1, Block par2Block, ItemStack par3ItemStack)
    {
        super(par1, par2Block);
        this.setMaxDamage(0);
        this.itemleft = par3ItemStack;
    }

    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
        super.onItemUse(par1ItemStack, par2EntityPlayer, par3World, par4, par5, par6, par7, par8, par9, par10);

        if (itemleft != null && !par2EntityPlayer.capabilities.isCreativeMode)
        {
            par2EntityPlayer.inventoryContainer.putStackInSlot(par2EntityPlayer.inventory.currentItem + 36, itemleft);
        }

        return true;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister icon)
    {
        this.itemIcon = icon.registerIcon("huornforest:" + this.getUnlocalizedName().replace("item.", ""));
    }
}
