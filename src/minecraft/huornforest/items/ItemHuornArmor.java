package huornforest.items;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import huornforest.common.ArmorHook.Effect;
import huornforest.common.HuornForest;
import huornforest.common.ArmorHook;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;

public class ItemHuornArmor extends ItemArmor {

	private String type;
	
	public ItemHuornArmor(int par1, EnumArmorMaterial par2EnumArmorMaterial, int par3, int par4) {
		super(par1, par2EnumArmorMaterial, par3, par4);
		this.iconString = null;
	}
	
	public ItemHuornArmor setArmorType(String type) {
		switch (this.armorType){
			case 0:
				this.setUnlocalizedName(type + "_helmet");
				break;
			case 1:
				this.setUnlocalizedName(type + "_chestplate");
				break;
			case 2:
				this.setUnlocalizedName(type + "_leggings");
				break;
			case 3:
				this.setUnlocalizedName(type + "_boots");
				break;
		}
		this.type = type;
		return this;
	}

    @Override
    @SideOnly(Side.CLIENT)
    public EnumRarity getRarity(ItemStack par1ItemStack){
		if(this.getArmorMaterial() == HuornForest.aRUBY || this.getArmorMaterial() == HuornForest.aOBSIDIAN) {
			return EnumRarity.epic;
		} else {
			return EnumRarity.common;
		}
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
    	String def = "";
    	Effect effect = ArmorHook.getArmorEffect(this.getArmorMaterial());
		if(effect == ArmorHook.Effect.CONDUCTOR) {
			def = "tool.conductor.effect";
		} else if(effect == ArmorHook.Effect.MAGNET) {
			def = "tool.magnet.effect";
		} else if(effect == ArmorHook.Effect.OBSIDIAN) {
			def = "tool.obsidian.effect";
    	}
		if (effect != null) {
	        par3List.add(EnumChatFormatting.GRAY.ITALIC + "\"" + StatCollector.translateToLocal(def) + "\"");
		}
    }
    
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
        super.registerIcons(par1IconRegister);
        this.itemIcon = par1IconRegister.registerIcon("huornforest:" + this.getUnlocalizedName().replace("item.", ""));
    }
    
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		if (type == "overlay") {
			return "huornforest:textures/models/armor/" + this.type + "_layer_2.png";
		} else {
			return "huornforest:textures/models/armor/" + this.type + "_layer_1.png";
		}
	}
}