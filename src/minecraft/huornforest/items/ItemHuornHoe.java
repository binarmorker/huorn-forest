package huornforest.items;

import huornforest.common.ToolsHook;
import huornforest.common.HuornForest;
import huornforest.common.ToolsHook.Effect;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class ItemHuornHoe extends ItemHoe
{
	public final EnumToolMaterial theToolMaterial;
	
    public ItemHuornHoe(int par1, EnumToolMaterial par2)
    {
        super(par1, par2);
    	this.theToolMaterial = par2;
        this.setMaxStackSize(1);
    }

    @Override
    public boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z, EntityPlayer player)
    {
    	return ToolsHook.onBlockStartBreak(this.theToolMaterial, itemstack, X, Y, Z, player);
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public EnumRarity getRarity(ItemStack par1ItemStack){
		if(theToolMaterial == HuornForest.PEARL || theToolMaterial == HuornForest.MAGMA) {
			return EnumRarity.uncommon;
		} else if(theToolMaterial == HuornForest.LIVINGWOOD || theToolMaterial == HuornForest.LIVINGMETAL) {
			return EnumRarity.rare;
		} else if(theToolMaterial == HuornForest.RUBY || theToolMaterial == HuornForest.AMETHYST || theToolMaterial == HuornForest.OBSIDIAN) {
			return EnumRarity.epic;
		} else {
			return EnumRarity.common;
		}
    }

    @Override
    public boolean hasEffect(ItemStack par1ItemStack){
    	return this.getRarity(par1ItemStack) == EnumRarity.rare;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
    	String def = "";
    	Effect effect = ToolsHook.getToolEffect(theToolMaterial);
		if(effect == ToolsHook.Effect.FORTUNE) {
			def = "tool.fortune.effect";
		} else if(effect == ToolsHook.Effect.SMELT) {
			def = "tool.smelt.effect";
		} else if(effect == ToolsHook.Effect.TRANSFUSION) {
			def = "tool.transfusion.effect";
    	}
		if (effect != null) {
	        par3List.add(EnumChatFormatting.GRAY.ITALIC + "\"" + StatCollector.translateToLocal(def) + "\"");
		}
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.itemIcon = par1IconRegister.registerIcon("huornforest:" + this.getUnlocalizedName().replace("item.", ""));
    }
}
