package huornforest.items;

import huornforest.common.HuornForest;
import huornforest.common.HuornPotionHook;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionHelper;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class ItemHuornPotion extends ItemFood
{
    public static final String[] itemMetaNames = new String[]
    {
        "creeper",
        "creeper1",
        "creeper2",
        "napalm",
        "napalm1",
        "napalm2",
        "jesus",
        "jesus1",
        "jesus2",
        "ender",
        "ender1",
        "ender2"
    };
    public static final String[] item_a = new String[]
    {
        "potionCreeper",
        "potionNapalm",
        "potionJesus",
        "potionEnder"
    };
    @SideOnly(Side.CLIENT)
    private Icon[] item_b;
    public static List<Integer> ingredients = new ArrayList<Integer>();

    public ItemHuornPotion(int id)
    {
        super(id, 0, 0, false);
        this.setMaxStackSize(1);
        this.setHasSubtypes(true);
        this.setMaxDamage(0);
    }

    /**
     * Methods to put in my future new Brewing Stand Class
     */

    public ItemHuornPotion addIngredient(int itemId)
    {
        this.ingredients.add(itemId);
        return this;
    }

    public ItemHuornPotion delIngredient(int itemId)
    {
        this.ingredients.remove(itemId);
        return this;
    }

    public int[] getIngredients()
    {
        int[] in = new int[this.ingredients.size()];

        for (int i = 0; i < this.ingredients.size(); i++)
        {
            in[i] = this.ingredients.get(i);
        }

        return in;
    }

    /**
     * End of these methods
     */

    @SideOnly(Side.CLIENT)
    public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List)
    {
        for (int j = 0; j < itemMetaNames.length; ++j)
        {
            par3List.add(new ItemStack(par1, 1, j));
        }
    }

    @SideOnly(Side.CLIENT)
    public Icon getIconFromDamage(int par1)
    {
        int j = MathHelper.clamp_int(par1, 0, itemMetaNames.length - 1);
        return item_b[j];
    }

    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {
    	String def = this.getUnlocalizedName() + "." + itemMetaNames[par1ItemStack.getItemDamage()] + ".effect";
        if (par1ItemStack.getItemDamage() / 3 == 0 || par1ItemStack.getItemDamage() / 3 == 1)
            par3List.add(EnumChatFormatting.RED + "" + StatCollector.translateToLocal(def));
        else
            par3List.add("" + StatCollector.translateToLocal(def));
    }

    public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (!par3EntityPlayer.capabilities.isCreativeMode)
        {
            --par1ItemStack.stackSize;
        }

        if (!par2World.isRemote)
        {
            int force = par1ItemStack.getItemDamage() % 3;

            if (par1ItemStack.getItemDamage() / 3 == 0)
            {
                par3EntityPlayer.addPotionEffect(new PotionEffect(HuornForest.creeper.id, 3000 * (force + 1), force));
            }
            else if (par1ItemStack.getItemDamage() / 3 == 1)
            {
                par3EntityPlayer.addPotionEffect(new PotionEffect(HuornForest.napalm.id, 200 * (force + 1), force));
            }
            else if (par1ItemStack.getItemDamage() / 3 == 2)
            {
                par3EntityPlayer.addPotionEffect(new PotionEffect(HuornForest.jesus.id, 600 * (force + 1), force));
            }
            else if (par1ItemStack.getItemDamage() / 3 == 3)
            {
            	HuornPotionHook.randomTeleport(par3EntityPlayer, force + 1);
            }
        }

        if (!par3EntityPlayer.capabilities.isCreativeMode)
        {
            if (par1ItemStack.stackSize <= 0)
            {
                return new ItemStack(Item.glassBottle);
            }

            par3EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.glassBottle));
        }

        return par1ItemStack;
    }

    public EnumAction getItemUseAction(ItemStack par1ItemStack)
    {
        return EnumAction.drink;
    }

    public int getMaxItemUseDuration(ItemStack par1ItemStack)
    {
        return 32;
    }

    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
        return par1ItemStack;
    }
    
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
        return false;
    }

    public String getUnlocalizedName(ItemStack par1ItemStack)
    {
        int i = MathHelper.clamp_int(par1ItemStack.getItemDamage(), 0, itemMetaNames.length - 1);
        return super.getUnlocalizedName() + "." + item_a[i/3];
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.item_b = new Icon[itemMetaNames.length];

        for (int i = 0; i < itemMetaNames.length; ++i)
        {
            this.item_b[i] = par1IconRegister.registerIcon("huornforest:" + item_a[i/3]);
        }
    }
}
