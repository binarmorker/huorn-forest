package huornforest.items;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemMultiCombinable extends Item
{
    public ItemMultiCombinable(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setMaxDamage(0);
    }

    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (par1ItemStack.stackSize >= 8 
        && !par3EntityPlayer.capabilities.isCreativeMode
        && par1ItemStack.getItemDamage() > 0 
        && par3EntityPlayer.inventory.addItemStackToInventory(new ItemStack(par1ItemStack.itemID, 1, par1ItemStack.getItemDamage()-1))) {
        	par1ItemStack.stackSize -= 8;
        }
        return par1ItemStack;
    }
}
