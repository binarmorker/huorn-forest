package huornforest.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemCommon extends Item
{
    public ItemCommon(int par1)
    {
        super(par1);
        this.setMaxDamage(0);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister icon)
    {
        this.itemIcon = icon.registerIcon("huornforest:" + this.getUnlocalizedName().replace("item.", ""));
    }
}
